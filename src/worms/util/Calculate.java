package worms.util;

import worms.model.GameObject;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
public class Calculate {
    
	/**
     * Returns an integer equal to the given value rounded towards zero.
     * If val (rounded) is larger than Integer.MAX_VALUE, Integer.MAX_VALUE is returned.
     * If it is smaller than Integer.MIN_VALUE, Integer.MIN_VALUE will be returned.
     * @param val The value to round.
     * @return  val rounded towards zero
     */
    public static int roundToZero(double val) {
        double rounded;
        if (val > 0) {
            rounded = Math.floor(val);
            if (rounded > Integer.MAX_VALUE)
                return Integer.MAX_VALUE;
        } else {
            rounded = Math.ceil(val);
            if (rounded < Integer.MIN_VALUE)
                return Integer.MIN_VALUE;
        }
        
        return (int) rounded;
        
    }
	
	/**
	 * Returns the distance between two points.
	 */
	public static double distanceBetween(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}

	/**
	 * Returns a boolean indicating if two gameobjects overlap or not.
	 */
	public static boolean overlaps(GameObject o1, GameObject o2) {
		return distanceBetween(o1.getXCoordinate(), o1.getYCoordinate(), o2.getXCoordinate(), o2.getYCoordinate()) <= o1.getRadius() + o2.getRadius();
	}
	
	/**
	 * Returns a boolean indicating if two gameobjects overlap or not.
	 */
	public static boolean overlaps(GameObject o1, double x2, double y2, double radius2) {
		return overlaps(o1.getXCoordinate(), o1.getYCoordinate(), o1.getRadius(), x2, y2, radius2);
	}
	
	/**
	 * Returns a boolean indicating if two gameobjects overlap or not.
	 */
	public static boolean overlaps(double x1, double y1, double radius1, double x2, double y2, double radius2) {
		return distanceBetween(x1, y1, x2, y2) <= radius1 + radius2;
	}
}
