package worms.model;

import worms.util.Calculate;

/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 */
public class Projectile extends OrientedGameObject {
	/**
	 * Create a new projectile at the given location, 
	 * with the given orientation, radius, yield, fired by parentWeapon.
	 * @effect	Call the super constructor.
	 * 			| super(xCoordinate, yCoordinate, orientation, radius);
	 * @param 	xCoordinate
	 * 			| The x-coordinate (horizontal) of this projectile.
	 * @param 	yCoordinate
	 * 			| The y-coordinate (vertical) of this projectile.
	 * @param 	orientation
	 * 			| The direction of this projectile, expressed as an angle in radians.
				  For example, the angle of a projectile facing right is 0, a projectile facing up is at
				  angle PI/2, a projectile facing left is at angle PI and a projectile facing down is at angle
				  3PI/2.
	 * @param 	radius
	 * 			| The radius of this projectile (expressed in meters) centered on the projectile's position.
	 * @throws	IllegalArgumentException
	 * 			| If xCoordinate, yCoordinate or radius are invalid.
	 */
	public Projectile(double xCoordinate, double yCoordinate, double orientation, 
			int yield, Weapon parentWeapon, World world) throws IllegalArgumentException 
	{
		super(xCoordinate, yCoordinate, orientation, parentWeapon.getRadius(), world);
		assert isValidYield(yield);
		this.yield = yield;
		this.parentWeapon = parentWeapon;
	}
	
	
	
	public void jump(double timeStep) throws IllegalStateException {
		super.jump(timeStep);
		
		for (Worm worm : this.getWorld().getWorms())
			if (Calculate.overlaps(worm, this.getXCoordinate(), this.getYCoordinate(), this.getRadius()))
				worm.hurt(this.getDamage());
		
		this.getWorld().removeActiveProjectile();
	}
	
	/**
	 * Returns the total amount of time (in seconds) that a
	 * jump of the given projectile would take.
	 * @param timeStep An elementary time interval during which you may assume
	 *                 that the projectile will not completely move through a piece of impassable terrain.
	 * @return	Amount of time in seconds a jump of this worm would take.
	 * 			| result == getDistance()/(getStartingSpeed() * Math.cos(orientation));
	 */
	public double getJumpTime(double timeStep) throws IllegalStateException {
		World world = this.getWorld();
		double jumpTime = 0;
		double[] jumpStep = {this.getXCoordinate(), this.getYCoordinate()};
		while (world.isPassable(jumpStep[0], jumpStep[1], this.getRadius())) 
		{
			if (world.isOutsideWorld(jumpStep[0], jumpStep[1], this.getRadius()))
				return jumpTime;
			
			jumpTime += timeStep;
			jumpStep = this.getJumpStep(jumpTime);
			
			for (Worm worm : world.getWorms())
				if (Calculate.overlaps(worm, jumpStep[0], jumpStep[1], this.getRadius())) {
					return jumpTime;
				}
		}
		
		return jumpTime;
	}
	
	/**
	 * Get the mass of this projectile.
	 */
	double getMass()
	{
		return parentWeapon.getMass();
	}
	/**
	 * Get the launch force of this projectile.
	 * @return 	Get the launch force which is dependant on the propulsion yield
	 * 			at which the projectile was fired.
	 */
	double getForce()
	{		
		double minForce = parentWeapon.getMinForce();
		double maxForce = parentWeapon.getMaxForce();
		if (minForce == maxForce)
			return minForce;
		return minForce + (maxForce - minForce) * ((double)yield * 0.01);
	}
	
	/**
	 * Returns a boolean indicating if the given propulsion yield is valid.
	 * A given propulsion yield is bigger than or equal to 0 and smaller than
	 * or equal to 100.
	 * @param yield	The propulsion yield to check.
	 * @return | yield >= 0 && yield <= 100
	 */
	public static boolean isValidYield(int yield) {
		return yield >= 0 && yield <= 100;
	}
	
	private final int yield;
	
	/**
	 * @return The damage in hitpoints which this projectile deals upon impact with a worm.
	 */
	public double getDamage()
	{
		return parentWeapon.getDamage();
	}
	
	/* (non-Javadoc)
	 * @see worms.model.GameObject#getRadius()
	 */
	public double getRadius() 
	{
		return parentWeapon.getRadius();
	}
	
	Weapon parentWeapon;
	
	/**
	 * @return True if this projectile is the active projectile in world, false otherwise.
	 * 			| result == (this.getWorld().getActiveProjectile() == this)
	 */
	public boolean isActive() {
		return this.getWorld().getActiveProjectile() == this;

	}
	
}
