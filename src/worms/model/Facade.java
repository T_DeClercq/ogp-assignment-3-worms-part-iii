package worms.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import worms.gui.game.IActionHandler;
import worms.model.programs.Expression;
import worms.model.programs.ParseOutcome;
import worms.model.programs.ProgramFactoryImpl;
import worms.model.programs.ProgramParser;
import worms.model.programs.Statement;
import worms.model.programs.Type;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
public class Facade implements IFacade {
	
	public Facade() {
		defaultNames = new ArrayList<>(Arrays.asList("Eddy Vedder", "Tim McVeigh", "Danny", "Sarah", 
				"Anna", "Samantha", "O'Reilly", "McLeary", "Kathy", "Brian Adam", "Tom Jonesey", 
				"Harry Ford", "Lady D", "Julie Robert", "Jenna", "Sam", "Hannah", "Napoleon", "Hannibal",
				"Caesar", "King", "Mary", "Washington", "Lincoln", "Amanda", "Emily", "Doc", "Speedy"));
	}
	
	
	@Override
	public void addEmptyTeam(World world, String newName) {
		try {
			world.addEmptyTeam(newName);
		} catch (IllegalArgumentException ex) {
			throw new ModelException("Tried adding a team with an invalid name.", ex);
		}
	}

	@Override
	public void addNewFood(World world) {
		double[] position = world.getRandomAdjacentPos(Food.getStandardRadius());
		world.addFood(createFood(world, position[0], position[1]));
	}
	
	private String getRandomDefaultName() {
		if (defaultNames.isEmpty())
			return "Teddy";
		
		return defaultNames.remove(new Random().nextInt(defaultNames.size()));
	}
	
	
	private ArrayList<String> defaultNames;
	
	
	
	@Override
	public boolean canFall(Worm worm) {
		return worm.canFall();
	}

	@Override
	public boolean canMove(Worm worm) {
		return worm.canMove();
	}

	@Override
	public boolean canTurn(Worm worm, double angle) {
		return worm.canTurn(angle);
	}

	@Override
	public Food createFood(World world, double x, double y) {
		return world.createFood(x, y);
	}

	@Override
	public World createWorld(double width, double height,
			boolean[][] passableMap, Random random) {
		World world = new World(width,height,passableMap,random);
		return world;
	}

	@Override
	public void fall(Worm worm) {
		worm.fall();
	}

	@Override
	public int getActionPoints(Worm worm) {
		return worm.getActionPoints();
	}

	@Override
	public Projectile getActiveProjectile(World world) {
		return world.getActiveProjectile();
	}

	@Override
	public Worm getCurrentWorm(World world) {
		return world.getCurrentWorm();
	}

	@Override
	public Collection<Food> getFood(World world) {
		return world.getFood();
	}

	@Override
	public int getHitPoints(Worm worm) {
		return worm.getHitPoints();
	}

	@Override
	public double[] getJumpStep(Projectile projectile, double t) {
		return projectile.getJumpStep(t);
	}

	@Override
	public double[] getJumpStep(Worm worm, double t) {
		return worm.getJumpStep(t);
	}

	@Override
	public double getJumpTime(Projectile projectile, double timeStep) {
		return projectile.getJumpTime(timeStep);
	}

	@Override
	public double getJumpTime(Worm worm, double timeStep) throws ModelException {
		try {
			return worm.getJumpTime(timeStep);
		} catch (IllegalStateException ex) {
			throw new ModelException(ex);
		}
	}

	@Override
	public double getMass(Worm worm) {
		return worm.getMass();
	}

	@Override
	public int getMaxActionPoints(Worm worm) {
		return worm.getMaxActionPoints();
	}

	@Override
	public int getMaxHitPoints(Worm worm) {
		return worm.getMaxHitPoints();
	}

	@Override
	public double getMinimalRadius(Worm worm) {
		return worm.getMinimalRadius();
	}

	@Override
	public String getName(Worm worm) {
		return worm.getName();
	}

	@Override
	public double getOrientation(Worm worm) {
		return worm.getOrientation();
	}

	@Override
	public double getRadius(Food food) {
		return food.getRadius();
	}

	@Override
	public double getRadius(Projectile projectile) {
		return projectile.getRadius();
	}

	@Override
	public double getRadius(Worm worm) {
		return worm.getRadius();
	}

	@Override
	public String getSelectedWeapon(Worm worm) {
		return worm.getSelectedWeapon();
	}

	@Override
	public String getTeamName(Worm worm) {
		return worm.getTeamName();
	}

	@Override
	public String getWinner(World world) {
		return world.getWinner();
	}

	@Override
	public Collection<Worm> getWorms(World world) {
		return world.getWorms();
	}

	@Override
	public double getX(Food food) {
		return food.getXCoordinate();
	}

	@Override
	public double getX(Projectile projectile) {
		return projectile.getXCoordinate();
	}

	@Override
	public double getX(Worm worm) {
		return worm.getXCoordinate();
	}

	@Override
	public double getY(Food food) {
		return food.getYCoordinate();
	}

	@Override
	public double getY(Projectile projectile) {
		return projectile.getYCoordinate();
	}

	@Override
	public double getY(Worm worm) {
		return worm.getYCoordinate();
	}

	@Override
	public boolean isActive(Food food) {
		return food.isActive();
	}

	@Override
	public boolean isActive(Projectile projectile) {
		return projectile.isActive();
	}

	@Override
	public boolean isAdjacent(World world, double x, double y, double radius) {
		return world.isAdjacent(x, y, radius);
	}

	@Override
	public boolean isAlive(Worm worm) {
		return worm.isAlive();
	}

	@Override
	public boolean isGameFinished(World world) {
		return world.isGameFinished();
	}
	@Override
	public boolean isImpassable(World world, double x, double y, double radius) {
		return world.isImpassable(x, y, radius);
	}

	@Override
	public void jump(Projectile projectile, double timeStep) {
		try {
			projectile.jump(timeStep);
		} catch (IllegalStateException ex) {
			throw new ModelException(ex);
		}
	}

	@Override
	public void jump(Worm worm, double timeStep) throws ModelException {
		try {
			worm.jump(timeStep);
		} catch (IllegalStateException ex) {
			throw new ModelException(ex);
		}
	}

	@Override
	public void move(Worm worm) {
		try {
			worm.move();
		} catch (IllegalArgumentException exception) {
			throw new ModelException(exception);
		} catch (IllegalStateException exception) {
			throw new ModelException(exception);
		}
	}

	@Override
	public void rename(Worm worm, String newName) throws ModelException {
		try {
			worm.setName(newName);
		} catch (IllegalArgumentException ex) {
			throw new ModelException(ex);
		}
	}

	@Override
	public void selectNextWeapon(Worm worm) {
		worm.selectNextWeapon();
		
	}

	@Override
	public void setRadius(Worm worm, double newRadius) throws ModelException {
		try {
			worm.setRadius(newRadius);
		} catch (IllegalArgumentException ex) {
			throw new ModelException(ex);
		}
		
	}

	@Override
	public void shoot(Worm worm, int yield) throws ModelException {
		try {
			worm.shoot(yield);
		} catch (IllegalStateException ex) {
			throw new ModelException(ex);
		}
	}

	@Override
	public void startGame(World world) {
		try {
			world.startGame();
		} catch (IllegalStateException ex) {
			throw new ModelException(ex);
		}
	}

	@Override
	public void startNextTurn(World world) throws ModelException {
		try {
			world.startNextTurn();
		} catch (IllegalStateException ex) {
			throw new ModelException(ex);
		}
	}

	@Override
	public void turn(Worm worm, double angle) {
		worm.turn(angle);
		
	}

	@Override
	public void addNewWorm(World world, Program program) {
		double radius = 0.3;
		double[] position = world.getRandomAdjacentPos(radius);
		Worm worm = createWorm(world, position[0], position[1], 
				new Random().nextInt(2) * Math.PI, radius, getRandomDefaultName(), program);
		world.addWorm(worm);
	}
	
	@Override
	public Worm createWorm(World world, double x, double y, double direction,
			double radius, String name, Program program) {
				return world.createWorm(x, y, direction,radius, name, program);
	}


	@Override
	public ParseOutcome<?> parseProgram(String programText,	IActionHandler handler) {
		ProgramParser<Expression<?>,Statement,Type> parser = new ProgramParser<>(new ProgramFactoryImpl());
		try {
			parser.parse(programText);
		} catch (RuntimeException ex) {
			ArrayList<String> errors = new ArrayList<String>();
			String error = ex.getMessage() == null ? ex.getCause().getMessage() : ex.getMessage();
			errors.add(error);
			return ParseOutcome.failure(errors);
		}
		if (!parser.getErrors().isEmpty())
			return ParseOutcome.failure(parser.getErrors());
		
		Program program = new Program(parser.getGlobals(), parser.getStatement(), handler);
		return ParseOutcome.success(program);
	}


	@Override
	public boolean hasProgram(Worm worm) {
		return worm.getProgram() != null;
	}


	@Override
	public boolean isWellFormed(Program program) {
		return program.isWellFormed();
	}
	

}
