package worms.model;

import be.kuleuven.cs.som.annotate.*;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 *
 */
public abstract class OrientedGameObject extends GameObject {

	
	
	/**
	 * Create a game object with the given coordinates, orientation, radius and world.
	 * @param orientation The orientation of this object.
	 * @param world	The world this object will exist in.
	 * @throws IllegalArgumentException	
	 * 			When world is null.
	 * 			| world == null
	 * @effect	This objects orientation will be set to orientation.
	 * 			| this.setOrientation(orientation)
	 * @post	This object will have a reference to the world it will be added to.
	 * 			| new.getWorld() == world
	 */
	public OrientedGameObject(double xCoordinate, double yCoordinate, double orientation, double radius, World world) throws IllegalArgumentException 
	{
		super(xCoordinate, yCoordinate, radius);
		if (world == null)
			throw new IllegalArgumentException();
		this.setOrientation(orientation);
		this.world = world;
	}
	
	/**
	 * @return	The world this object exists in.
	 */
	public World getWorld() {
		return this.world;
	}
	
	private final World world;

	
	
	// Aspects related to direction: Nominally
	/**
	 * Returns the orientation of the object.
	 * 
	 * @return	A double with the radian value of the orientation of the object.
	 * 			| result == object.orientation
	 */
	@Basic @Raw
	public double getOrientation() {
		return this.orientation;
	}
	
	
	/**
	 * Change the orientation of the object to the given orientation.
	 * @pre		orientation is a valid orientation
	 * 			| this.isValidorientation(orientation)
	 * @post	The new orientation of this object is the given orientation.
	 * 			| new.getOrientation() == orientation
	 * @param 	orientation
	 * 			An angle in radians to be the new orientation of the object.
	 */
	@Raw
	public void setOrientation(double orientation) {	
		assert isValidOrientation(orientation);
		this.orientation = orientation;
	}
	
	
	/**
	 * Checks if the given orientation is a valid orientation for any object.
	 * @param 	orientation
	 * 			An angle in radians, to be checked if it is a valid orientation.
	 * @return 	True if the orientation is a valid number and larger than or equal to -2*PI and smaller than 2*PI. False otherwise.
	 * 			| result == (orientation >= -PI * 2.0 && orientation < PI * 2.0) 
	 */
	public static boolean isValidOrientation(double orientation) {
		return isValidNumber(orientation) && 
				orientation >= -Math.PI * 2.0 &&
				orientation <= Math.PI * 2.0;
	}
	
	private double orientation;
	
	/**
	 * Get the time (in seconds) a jump of this object in the direction it is facing will take.
	 * Will throw an exception if the object cannot jump.
	 * @return 	The total time a jump will take, i.e. the time it takes for this object when it
	 * 			jumps before it hits something to end the jump.
	 * @throws IllegalStateException
	 */
	public abstract double getJumpTime(double timeStep) throws IllegalStateException;
	
	/**
	 * Makes the given object jump.
	 * @post	X and y coordinates will not change if jumpTime is 0
	 * 			| if (this.getJumpTime(timeStep) == 0) then
	 * 			| 	new.getXCoordinate() == this.getXCoordinate
	 * 			| 	new.getYCoordinate() == this.getYCoordinate
	 * @post	The x and y coordinate of this object will be equal to the coordinates
	 * 			found by this.getJumpStep(double) with as argument the total jumptime
	 * 			of this object.
	 * 			| new.getXCoordinate() == this.getJumpStep(this.getJumpTime(timeStep))[0]
	 * 			| new.getYCoordinate() == this.getJumpStep(this.getJumpTime(timeStep))[1]
	 */
	public void jump(double timeStep) throws IllegalStateException {
		double jumpTime = this.getJumpTime(timeStep);
		if (jumpTime == 0)
			return;
		
		double[] newPos = this.getJumpStep(jumpTime);
		this.setXCoordinate(newPos[0]);
		this.setYCoordinate(newPos[1]);
	}
	
	/**
	 * Returns the location on the jump trajectory of the given worm
	 * after a time t.
	 *  
	 * @param 	deltaT 
	 * 			The time to get the position for in the jump (in seconds).
	 * @return 	An array with two elements,
	 *  		with the first element being the x-coordinate and
	 *  		the second element the y-coordinate
	 *  		| result == {this.getXCoordinate() + (this.getStartingSpeed() * cos(this.getOrientation()) * deltaT)
	 *  		|			, this.getYCoordinate() + (this.getStartingSpeed() * sin(this.getOrientation()) * deltaT - 0.5 * this.getStandardAccel() * deltaT^2}
	 */
	public double[] getJumpStep(double deltaT) throws IllegalArgumentException {
		double startingSpeed = this.getStartingSpeed();
		double xSpeed =  startingSpeed * Math.cos(getOrientation());
		double ySpeed = startingSpeed * Math.sin(getOrientation());
		double xDeltaT = this.getXCoordinate() + (xSpeed * deltaT);
		double yDeltaT = this.getYCoordinate() + (ySpeed * deltaT - 0.5 * getStandardAccel() * deltaT * deltaT);
		
		double[] computedPosition = {xDeltaT, yDeltaT};
		return computedPosition; 
	}
	
	/**
	 * The standard acceleration on earth, used for calculations involving physics.
	 */
	@Basic
	public double getStandardAccel() {
		return standardAccel;
	}

	/**
	 * The standard acceleration on earth, used for calculations involving physics.
	 */
	private final double standardAccel =  9.80665;
	
	private double getStartingSpeed() {
		return (this.getForce() / this.getMass()) * 0.5;
	}
	
	abstract double getForce();
	
	abstract double getMass();
	
}