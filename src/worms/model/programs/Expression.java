package worms.model.programs;

import worms.model.GameObject;
import worms.model.Worm;
import worms.model.Food;
import worms.model.exceptions.FatalExecutionException;
import worms.util.Calculate;
import worms.util.Util;

//------------------------------------------------
//------------------------------------------------
//----------		BOOLEANS			----------
//------------------------------------------------
//------------------------------------------------

/**
 * An expression consisting of a single boolean value.
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 */
class ExpressionBooleanLiteral extends BooleanExpression {

	public ExpressionBooleanLiteral(boolean value, int line, int column) {
		super(line, column);
		this.value = value;
	}
	
	@Override
	public Boolean eval(VariableMap globals) {
		return this.value;
	}
	
	private boolean value;
	
}

/**
 * An expression that is the conjunction of two boolean expressions.
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 */
class ExpressionOfConjunction extends BooleanExpression {
	/**
	 * 
	 * @param e1
	 * @param e2
	 */
	public ExpressionOfConjunction(BooleanExpression e1, BooleanExpression e2, int line, int column) {
		super(line, column);
		this.expression1 = e1;
		this.expression2 = e2;
	}
	
	@Override
	public Boolean eval(VariableMap globals) {
		return expression1.eval(globals) && expression2.eval(globals);
	}
	
	private BooleanExpression expression1;
	private BooleanExpression expression2;
}

/**
 * An expression that is the disjunction of two boolean expressions.
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 */
class ExpressionOfDisjunction extends BooleanExpression {
	/**
	 * 
	 * @param e1
	 * @param e2
	 */
	public ExpressionOfDisjunction(BooleanExpression e1, BooleanExpression e2, int line, int column) {
		super(line, column);
		this.expression1 = e1;
		this.expression2 = e2;
	}
	
	@Override
	public Boolean eval(VariableMap globals) {
		return expression1.eval(globals) || expression2.eval(globals);
	}
	
	private BooleanExpression expression1;
	private BooleanExpression expression2;
}

/**
 * An expression that is the negation of a boolean expression.
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 */
class ExpressionOfNegation extends BooleanExpression {
	/**
	 * 
	 * @param e
	 */
	public ExpressionOfNegation(BooleanExpression e, int line, int column) {
		super(line, column);
		this.expression = e;
	}
	
	@Override
	public Boolean eval(VariableMap globals) {
		return !expression.eval(globals);
	}
	
	private BooleanExpression expression;
}

/**
 * A boolean expression that states if the given worm is in the same team as self.
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 */
class ExpressionSameTeam extends BooleanExpression {
	/**
	 * 
	 * @param other
	 */
	public ExpressionSameTeam(EntityExpression other, int line, int column) {
		super(line, column);
		this.other = other;
	}
	
	@Override
	public Boolean eval(VariableMap globals) throws FatalExecutionException {
		// evaluating other must return a worm
		if (!(other.eval(globals) instanceof Worm))
			throw new FatalExecutionException(getLine(), getColumn(),
					"Expected worm, but got food to check if it's in the same team.");
		Worm self = globals.getSelf();
		return self.getTeamName() != null && 
				((Worm)other.eval(globals)).getTeamName() == self.getTeamName();
	}
	
	private EntityExpression other;
}

/**
 * A boolean expression that evaluates if the given entity is a worm.
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 */
class ExpressionIsWorm extends BooleanExpression {
	/**
	 * 
	 * @param e
	 */
	public ExpressionIsWorm(EntityExpression e, int line, int column) {
		super(line, column);
		this.e = e;
	}
	
	@Override
	public Boolean eval(VariableMap globals) {
		GameObject entity = e.eval(globals);
		return entity != null && entity instanceof Worm;
	}
	
	private EntityExpression e;
}

/**
 * A boolean expression that evaluates if the given entity is a food ration.
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 */
class ExpressionIsFood extends BooleanExpression {
	/**
	 * 
	 * @param e
	 */
	public ExpressionIsFood(EntityExpression e, int line, int column) {
		super(line, column);
		this.e = e;
	}
	
	@Override
	public Boolean eval(VariableMap globals) {
		GameObject entity = e.eval(globals);
		return entity != null && entity instanceof Food;
	}
	
	private EntityExpression e;
}

//-----------------------
//----	COMPARISONS	-----
//-----------------------
/**
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class ExpressionLessThan extends ComparisonExpression {
	/**
	 * 
	 * @param d1
	 * @param d2
	 */
	public ExpressionLessThan(DoubleExpression d1, DoubleExpression d2, int line, int column) {
		super(d1, d2, line, column);
	}

	@Override
	public Boolean eval(VariableMap globals) {
		return d1.eval(globals) < d2.eval(globals);
	}
	
}
/**
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class ExpressionGreaterThan extends ComparisonExpression {
	/**
	 * 
	 * @param d1
	 * @param d2
	 */
	public ExpressionGreaterThan(DoubleExpression d1, DoubleExpression d2, int line, int column) {
		super(d1, d2, line, column);
	}

	@Override
	public Boolean eval(VariableMap globals) {
		return d1.eval(globals) > d2.eval(globals);
	}
	
}
/**
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class ExpressionLessThanOrEqualTo extends ComparisonExpression {
	/**
	 * 
	 * @param d1
	 * @param d2
	 */
	public ExpressionLessThanOrEqualTo(DoubleExpression d1, DoubleExpression d2, int line, int column) {
		super(d1, d2, line, column);
	}

	@Override
	public Boolean eval(VariableMap globals) {
		return Util.fuzzyLessThanOrEqualTo(d1.eval(globals), d2.eval(globals));
	}
	
}
/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class ExpressionGreaterThanOrEqualTo extends ComparisonExpression {
	/**
	 * 
	 * @param d1
	 * @param d2
	 */
	public ExpressionGreaterThanOrEqualTo(DoubleExpression d1, DoubleExpression d2, int line, int column) {
		super(d1, d2, line, column);
	}

	@Override
	public Boolean eval(VariableMap globals) {
		return Util.fuzzyGreaterThanOrEqualTo(d1.eval(globals), d2.eval(globals));
	}
	
}
/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class ExpressionEquality<E extends Expression<?>> extends BooleanExpression {
	/**
	 * 
	 * @param e1
	 * @param e2
	 */
	public ExpressionEquality(E e1, E e2, int line, int column) {
		super(line, column);
		this.e1 = e1;
		this.e2 = e2;
	}

	@Override
	public Boolean eval(VariableMap globals) {
		Type type = e1.getType();
		if (type == Type.DOUBLE)
			return Util.fuzzyEquals(((DoubleExpression)e1).eval(globals), ((DoubleExpression)e2).eval(globals));
		if (type == Type.BOOLEAN)
			return e1.eval(globals) == e2.eval(globals);
		GameObject o1 = ((EntityExpression)e1).eval(globals);
		if (o1 == null)
			return e2.eval(globals) == null; // could also return false if both objects are null, not sure what's expected
		return o1.equals(e2.eval(globals));
	}
	
	private E e1;
	private E e2;
}

// NOTE: Inequality is defined as the negation of equality
///**
// * 
// * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
// * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
// * @version	1.1
// *
// */
//class ExpressionInequality<E extends Expression<?>> extends BooleanExpression {
//	/**
//	 * 
//	 * @param e1
//	 * @param e2
//	 */
//	public ExpressionInequality(E e1, E e2, int line, int column) {
//		super(line, column);
//		this.e1 = e1;
//		this.e2 = e2;
//	}
//
//	@Override
//	public Boolean eval(VariableMap globals) {
//		Type type = e1.getType();
//		if (type != e2.getType())
//			return true;
//		if (type == Type.DOUBLE)
//			return !Util.fuzzyEquals(((DoubleExpression)e1).eval(globals), ((DoubleExpression)e2).eval(globals));
//		if (type == Type.BOOLEAN)
//			return e1.eval(globals) != e2.eval(globals);
//		GameObject o1 = ((EntityExpression)e1).eval(globals);
//		if (o1 == null)
//			return e2.eval(globals) != null;
//		return !o1.equals(e2.eval(globals));
//	}
//	
//	private E e1;
//	private E e2;
//}

/**
 * A boolean expression that evaluates if a given relation between
 * two double expressions is true. The relation can be less than, less
 * than or equal to, greater than, greater than or equal to, equality
 * or inequality.
 */
abstract class ComparisonExpression extends BooleanExpression {
	/**
	 * 
	 * @param d1
	 * @param d2
	 */
	public ComparisonExpression(DoubleExpression d1, DoubleExpression d2, int line, int column) {
		super(line, column);
		this.d1 = d1;
		this.d2 = d2;
	}
	
	DoubleExpression d1;
	DoubleExpression d2;
}

/**
 * An expression of type Boolean.
 */
abstract class BooleanExpression extends Expression<Boolean> {
	public BooleanExpression(int line, int column) {
		super(line, column);
	}
	@Override
	public abstract Boolean eval(VariableMap globals);
	@Override
	public Type getType() {
		return Type.BOOLEAN;
	}
}

//------------------------------------------------
//------------------------------------------------
//----------		DOUBLES				----------
//------------------------------------------------
//------------------------------------------------
/**
 * An expression consisting of a single double value.
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 */
class ExpressionDoubleLiteral extends DoubleExpression {
	/**
	 * 
	 * @param value
	 */
	public ExpressionDoubleLiteral(double value, int line, int column) {
		super(line, column);
		this.value = value;
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return this.value;
	}
	
	private double value;
}

//-----------------------
//----	ARITHMETICS	-----
//-----------------------
/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class ExpressionAdd extends ArithmeticExpression {
	/**
	 * 
	 * @param d1
	 * @param d2
	 */
	public ExpressionAdd(DoubleExpression d1, DoubleExpression d2, int line, int column) {
		super(d1, d2, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return d1.eval(globals) + d2.eval(globals);
	}
	
}
/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class ExpressionSubtraction extends ArithmeticExpression {
	/**
	 * 
	 * @param d1
	 * @param d2
	 */
	public ExpressionSubtraction(DoubleExpression d1, DoubleExpression d2, int line, int column) {
		super(d1, d2, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return d1.eval(globals) - d2.eval(globals);
	}
	
}
/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class ExpressionMul extends ArithmeticExpression {
	/**
	 * 
	 * @param d1
	 * @param d2
	 */
	public ExpressionMul(DoubleExpression d1, DoubleExpression d2, int line, int column) {
		super(d1, d2, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return d1.eval(globals) * d2.eval(globals);
	}
	
}
/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class ExpressionDivision extends ArithmeticExpression {
	/**
	 * 
	 * @param d1
	 * @param d2
	 */
	public ExpressionDivision(DoubleExpression d1, DoubleExpression d2, int line, int column) {
		super(d1, d2, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return d1.eval(globals) / d2.eval(globals);
	}
	
}

/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
abstract class ArithmeticExpression extends DoubleExpression {
	/**
	 * 
	 * @param d1
	 * @param d2
	 */
	public ArithmeticExpression(DoubleExpression d1, DoubleExpression d2, int line, int column) {
		super(line, column);
		this.d1 = d1;
		this.d2 = d2;
	}
	
	DoubleExpression d1;
	DoubleExpression d2;
}

//-----------------------
//----	   MATHS	-----
//-----------------------
/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class ExpressionSqrt extends MathsExpression {
	
	public ExpressionSqrt(DoubleExpression d, int line, int column) {
		super(d, line, column);
	}
	
	public Double eval(VariableMap globals) {
		return Math.sqrt(d.eval(globals));
	}
	
}

class ExpressionSin extends MathsExpression {
	
	public ExpressionSin(DoubleExpression d, int line, int column) {
		super(d, line, column);
	}
	
	public Double eval(VariableMap globals) {
		return Math.sin(d.eval(globals));
	}
}

class ExpressionCos extends MathsExpression {
	
	public ExpressionCos(DoubleExpression d, int line, int column) {
		super(d, line, column);
	}
	
	public Double eval(VariableMap globals) {
		return Math.cos(d.eval(globals));
	}
}

abstract class MathsExpression extends DoubleExpression {
	
	public MathsExpression(DoubleExpression d, int line, int column) {
		super(line, column);
		this.d = d;
	}
	
	DoubleExpression d;
}

//-----------------------
//----	  GETTERS	-----
//-----------------------

class ExpressionGetX extends GetterExpression {
	
	public ExpressionGetX(EntityExpression e, int line, int column) {
		super(e, line, column);
	}

	@Override
	public Double eval(VariableMap globals) {
		return entity(globals).getXCoordinate();
	}
}

class ExpressionGetY extends GetterExpression {
	
	public ExpressionGetY(EntityExpression e, int line, int column) {
		super(e, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return entity(globals).getYCoordinate();
	}
}

class ExpressionGetRadius extends GetterExpression {
	
	public ExpressionGetRadius(EntityExpression e, int line, int column) {
		super(e, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return entity(globals).getRadius();
	}
}

class ExpressionGetDir extends GetterWormExpression {
	
	public ExpressionGetDir(EntityExpression e, int line, int column) {
		super(e, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return entity(globals).getOrientation();
	}
}

class ExpressionGetAP extends GetterWormExpression {
	
	public ExpressionGetAP(EntityExpression e, int line, int column) {
		super(e, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return (double) entity(globals).getActionPoints();
	}
}

class ExpressionGetMaxAP extends GetterWormExpression {
	
	public ExpressionGetMaxAP(EntityExpression e, int line, int column) {
		super(e, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return (double) entity(globals).getMaxActionPoints();
	}
}

class ExpressionGetHP extends GetterWormExpression {
	
	public ExpressionGetHP(EntityExpression e, int line, int column) {
		super(e, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return (double) entity(globals).getHitPoints();
	}
}

class ExpressionGetMaxHP extends GetterWormExpression {
	
	public ExpressionGetMaxHP(EntityExpression e, int line, int column) {
		super(e, line, column);
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return (double) entity(globals).getMaxHitPoints();
	}
}

abstract class GetterWormExpression extends GetterExpression {

	public GetterWormExpression(EntityExpression e, int line, int column) {
		// evaluating e must return a worm
		super(e, line, column);
	}
	
	Worm entity(VariableMap globals) {
		// can throw a class cast exception at runtime!
		try {
			return (Worm) super.entity(globals);
		} catch (ClassCastException ex) {
			throw new FatalExecutionException(getLine(), getColumn(), ex, "Expected worm, but got food ration.");
		}
	}
	
}

abstract class GetterExpression extends DoubleExpression {
	
	public GetterExpression(EntityExpression e, int line, int column) {
		super(line, column);
		this.e = e;
	}
	
	GameObject entity(VariableMap globals) {
		return e.eval(globals);
	}
	
	protected EntityExpression e;
}

/**
 * An expression of type Double.
 */
abstract class DoubleExpression extends Expression<Double> {
	
	public DoubleExpression(int line, int column) {
		super(line, column);
	}
	@Override
	public abstract Double eval(VariableMap globals);
	@Override
	public Type getType() {
		return Type.DOUBLE;
	}
	
	/**
	 * Evaluate this double expression and return the outcome rounded towards zero.
	 * @return The result of evaluating this expression rounded to zero.
	 * 			| result = Util.roundToZero(eval())
	 */
	public int evalInt(VariableMap globals) {
		return Calculate.roundToZero(eval(globals));
	}
}

//------------------------------------------------
//------------------------------------------------
//----------		ENTITIES			----------
//------------------------------------------------
//------------------------------------------------

/**
 * An EntityExpression which holds the nullreference.
 */
class ExpressionNull extends EntityExpression {

	public ExpressionNull(int line, int column) {
		super(line, column);
	}

	@Override
	public GameObject eval(VariableMap globals) {
		return null;
	}
}

class ExpressionSelf extends EntityExpression {

	public ExpressionSelf(int line, int column) {
		super(line, column);
	}

	@Override
	public Worm eval(VariableMap globals) {
		return globals.getSelf();
	}
}

class ExpressionSearchObj extends EntityExpression {
	
	public ExpressionSearchObj(DoubleExpression d, int line, int column) {
		super(line, column);
		this.dir = d;
	}
	
	@Override
	public GameObject eval(VariableMap globals) {
		Worm self = globals.getSelf();
		double angle = dir.eval(globals) + self.getOrientation();
		double x = self.getXCoordinate() + Math.cos(angle) * (self.getRadius() + 0.0001);
		double y = self.getYCoordinate() + Math.sin(angle) * (self.getRadius() + 0.0001);
		return globals.getWorld().searchObject(x, y, angle);
	}
	
	private DoubleExpression dir;
}

class ExpressionEntityLiteral extends EntityExpression {
	public ExpressionEntityLiteral(GameObject e, int line, int column) {
		super(line, column);
		this.e = e;
	}
	
	@Override
	public GameObject eval(VariableMap globals) {
		return e;
	}
	
	private GameObject e;
}

/**
 * An expression of type Entity.
 */
abstract class EntityExpression extends Expression<GameObject> {
	
	public EntityExpression(int line, int column) {
		super(line, column);
	}
	@Override
	public abstract GameObject eval(VariableMap globals);
	@Override
	public Type getType() {
		return Type.ENTITY;
	}
}

class ExpressionBooleanVariable extends BooleanExpression {
	
	public ExpressionBooleanVariable(String varName, int line, int column) {
		super(line, column);
		this.varName = varName;
	}
	
	@Override
	public Boolean eval(VariableMap globals) {
		return (Boolean) globals.getValue(varName).eval(globals);
	}
	
	private String varName;
	
}

class ExpressionDoubleVariable extends DoubleExpression {
	
	public ExpressionDoubleVariable(String varName, int line, int column) {
		super(line, column);
		this.varName = varName;
	}
	
	@Override
	public Double eval(VariableMap globals) {
		return (Double) globals.getValue(varName).eval(globals);
	}
	
	private String varName;
	
}

class ExpressionEntityVariable extends EntityExpression {
	
	public ExpressionEntityVariable(String varName, int line, int column) {
		super(line, column);
		this.varName = varName;
	}
	
	@Override
	public GameObject eval(VariableMap globals) {
		return (GameObject) globals.getValue(varName).eval(globals);
	}
	
	private String varName;
	
}

/**
 * A certain expression that can be evaluated.
 * eval() can return a Boolean, a Double or an Entity.
 */
public abstract class Expression<T> extends PosHolder {
		
	public Expression(int line, int column) {
		super(line, column);
	}
	/**
	 * Get the type of this expression, which is the return type of eval()
	 * Can be any element of the Type enum (Type.BOOLEAN, Type.DOUBLE or Type.ENTITY)
	 * @return The type of this expression.
	 */
	public abstract Type getType();
	/**
	 * Evaluate the expression and get the result.
	 * @return  The result of this expression.
	 */
	public abstract T eval(VariableMap globals);
}
