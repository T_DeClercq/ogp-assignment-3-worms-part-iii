package worms.model.programs;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
public enum Type {
	BOOLEAN,
	DOUBLE,
	ENTITY;
}
