package worms.model.programs;
import java.util.HashMap;
import java.util.Map;

import be.kuleuven.cs.som.annotate.*;

import worms.model.World;
import worms.model.Worm;
import worms.model.exceptions.*;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
@Value
public class VariableMap {
	
	private final int ALLOWED_EXECUTIONS = 1000;
	
	public VariableMap(Map<String, Type> globalTypes, Worm self) {
		this.globalTypes = globalTypes;
		this.self = self;
		this.initialize();
	}
	
	/**
	 * Add all globals in globalTypes to globalValues.
	 * The values are initialized to be the following:
	 * 	if type is boolean: new ExpressionBooleanLiteral(false, 0, 0)
	 *  if type is double: new ExpressionDoubleLiteral(0, 0, 0)
	 *  if type is entity: new ExpressionNull(0, 0)
	 */
	private void initialize() {
		
		globalValues = new HashMap<>();
		
		for (String key : globalTypes.keySet()) {
			Type type = globalTypes.get(key);
			
			Expression<?> value;
			switch (type) {
			case BOOLEAN:
				value = new ExpressionBooleanLiteral(false, 0, 0);
				break;
			case DOUBLE:
				value = new ExpressionDoubleLiteral(0, 0, 0);
				break;
			case ENTITY:
				value = new ExpressionNull(0, 0);
				break;
			default:
				value = new ExpressionNull(0, 0);
				break;
			}
			
			globalValues.put(key, value);
		}
	}
	
	public void setValue(String key, Expression<?> value) 
				throws VariableNotDeclaredException, TypeMismatchException {
		if (getType(key) != value.getType())
			throw new TypeMismatchException(key, globalTypes.get(key), value.getType());
		
		globalValues.put(key, value);
	}
	
	public Expression<?> getValue(String key) throws VariableNotDeclaredException {
		if (!globalValues.containsKey(key))
			throw new VariableNotDeclaredException(key);
		return globalValues.get(key);
	}
	
	public Type getType(String varName) {
		return globalTypes.get(varName);
	}
	
	@Basic @Immutable
	public Worm getSelf() {
		return this.self;
	}

	public World getWorld() {
		return getSelf().getWorld();
	}
	
	public void restoreExecutions() {
		this.executionsLeft = ALLOWED_EXECUTIONS;
	}
	
	public void useExecution() {
		this.executionsLeft--;
	}
	
	public boolean hasExecutionsLeft() {
		return executionsLeft > 0;
	}
	
	private int executionsLeft;
	
	private final Worm self;
	private Map<String, Expression<?>> globalValues;
	private final Map<String, Type> globalTypes;
	

}
