package worms.model.programs;

import java.util.Map;
import java.util.Stack;

import worms.model.Worm;

public class WormProgramData {
	
	public WormProgramData(Worm self, Map<String, Type> globals) {
		this.programStatementStack = new Stack<>();
		this.varMap = new VariableMap(globals, self);
	}
	
	public Stack<Statement> getStatementStack() {
		return this.programStatementStack;
	}
	
	private Stack<Statement> programStatementStack;
	
	private final VariableMap varMap;

	public VariableMap getVarMap() {
		return this.varMap;
	}
	
}
