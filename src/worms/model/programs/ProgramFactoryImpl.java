package worms.model.programs;

import java.util.List;

import worms.model.exceptions.*;
import worms.model.programs.ProgramFactory;

/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
public class ProgramFactoryImpl implements ProgramFactory<Expression<?>, Statement, Type> {
	
	private BooleanExpression toBooleanExpression(Expression<?> e, int line, int column) throws IllegalTypeException {
		if (e.getType() != Type.BOOLEAN)
			throw new IllegalTypeException(e.getType(), Type.BOOLEAN, line, column);
		return (BooleanExpression) e;
	}
	
	private DoubleExpression toDoubleExpression(Expression<?> e, int line, int column) throws IllegalTypeException {
		if (e.getType() != Type.DOUBLE)
			throw new IllegalTypeException(e.getType(), Type.DOUBLE, line, column);
		return (DoubleExpression) e;
	}
	
	private EntityExpression toEntityExpression(Expression<?> e, int line, int column) throws IllegalTypeException {
		if (e.getType() != Type.ENTITY)
			throw new IllegalTypeException(e.getType(), Type.ENTITY, line, column);
		return (EntityExpression) e;
	}
	
	@Override
	public DoubleExpression createDoubleLiteral(int line, int column, double d) {
		// no need to further specify return type of this method, nor any other method in this class
		// we'll just return an instance of the datatype where getType() is implemented
		// i.e. DoubleExpression, BooleanExpression or EntityExpression
		return new ExpressionDoubleLiteral(d, line, column);
	}

	@Override
	public BooleanExpression createBooleanLiteral(int line, int column, boolean b) {
		return new ExpressionBooleanLiteral(b, line, column);
	}

	@Override
	public BooleanExpression createAnd(int line, int column, Expression<?> e1,
			Expression<?> e2) {
		return new ExpressionOfConjunction(toBooleanExpression(e1, line, column), toBooleanExpression(e2, line, column), line, column);
	}

	@Override
	public BooleanExpression createOr(int line, int column, Expression<?> e1,
			Expression<?> e2) {
		return new ExpressionOfDisjunction(toBooleanExpression(e1, line, column), toBooleanExpression(e2, line, column), line, column);
	}

	@Override
	public BooleanExpression createNot(int line, int column, Expression<?> e) {
		return new ExpressionOfNegation(toBooleanExpression(e, line, column), line, column);
	}

	@Override
	public EntityExpression createNull(int line, int column) {
		return new ExpressionNull(line, column);
	}
	
	@Override
	public EntityExpression createSelf(int line, int column) {
		return new ExpressionSelf(line, column);
	}

	@Override
	public DoubleExpression createGetX(int line, int column, Expression<?> e) {
		return new ExpressionGetX(toEntityExpression(e, line, column), line, column);
	}

	@Override
	public DoubleExpression createGetY(int line, int column, Expression<?> e) {
		return new ExpressionGetY(toEntityExpression(e, line, column), line, column);
	}

	@Override
	public DoubleExpression createGetRadius(int line, int column, Expression<?> e) {
		return new ExpressionGetRadius(toEntityExpression(e, line, column), line, column);
	}

	@Override
	public DoubleExpression createGetDir(int line, int column, Expression<?> e) {
		return new ExpressionGetDir(toEntityExpression(e, line, column), line, column);
	}

	@Override
	public DoubleExpression createGetAP(int line, int column, Expression<?> e) {
		return new ExpressionGetAP(toEntityExpression(e, line, column), line, column);
	}

	@Override
	public DoubleExpression createGetMaxAP(int line, int column, Expression<?> e) {
		return new ExpressionGetMaxAP(toEntityExpression(e, line, column), line, column);
	}

	@Override
	public DoubleExpression createGetHP(int line, int column, Expression<?> e) {
		return new ExpressionGetHP(toEntityExpression(e, line, column), line, column);
	}

	@Override
	public DoubleExpression createGetMaxHP(int line, int column, Expression<?> e) {
		return new ExpressionGetMaxHP(toEntityExpression(e, line, column), line, column);
	}

	@Override
	public BooleanExpression createSameTeam(int line, int column, Expression<?> e) {
		return new ExpressionSameTeam(toEntityExpression(e, line, column), line, column);
	}

	@Override
	public EntityExpression createSearchObj(int line, int column, Expression<?> e) {
		return new ExpressionSearchObj(toDoubleExpression(e, line, column), line, column);
	}

	@Override
	public BooleanExpression createIsWorm(int line, int column, Expression<?> e) {
		return new ExpressionIsWorm(toEntityExpression(e, line, column), line, column);
	}

	@Override
	public BooleanExpression createIsFood(int line, int column, Expression<?> e) {
		return new ExpressionIsFood(toEntityExpression(e, line, column), line, column);
	}
	
	@Override
	public Expression<?> createVariableAccess(int line, int column, String name) {
		return null;
	}

	@Override
	public Expression<?> createVariableAccess(int line, int column, String name, Type type) {
		if (type == Type.BOOLEAN)
			return new ExpressionBooleanVariable(name, line, column);
		if (type == Type.DOUBLE)
			return new ExpressionDoubleVariable(name, line, column);
		
		return new ExpressionEntityVariable(name, line, column);
	}

	@Override
	public BooleanExpression createLessThan(int line, int column, Expression<?> e1,
			Expression<?> e2) {
		return new ExpressionLessThan(toDoubleExpression(e1, line, column), toDoubleExpression(e2, line, column), line, column);
	}

	@Override
	public BooleanExpression createGreaterThan(int line, int column, Expression<?> e1,
			Expression<?> e2) {
		return new ExpressionGreaterThan(toDoubleExpression(e1, line, column), toDoubleExpression(e2, line, column), line, column);
	}

	@Override
	public BooleanExpression createLessThanOrEqualTo(int line, int column,
			Expression<?> e1, Expression<?> e2) {
		return new ExpressionLessThanOrEqualTo(toDoubleExpression(e1, line, column), toDoubleExpression(e2, line, column), line, column);
	}

	@Override
	public BooleanExpression createGreaterThanOrEqualTo(int line, int column,
			Expression<?> e1, Expression<?> e2) {
		return new ExpressionGreaterThanOrEqualTo(toDoubleExpression(e1, line, column), toDoubleExpression(e2, line, column), line, column);
	}

	@Override
	public BooleanExpression createEquality(int line, int column, Expression<?> e1,
			Expression<?> e2) {
		Type type = e1.getType();
		if (type == Type.BOOLEAN)
			return new ExpressionEquality<BooleanExpression>(
							toBooleanExpression(e1, line, column), toBooleanExpression(e2, line, column), line, column);
		
		if (type == Type.DOUBLE)
			return new ExpressionEquality<DoubleExpression>(
					toDoubleExpression(e1, line, column), toDoubleExpression(e2, line, column), line, column);
		
		return new ExpressionEquality<EntityExpression>(
				toEntityExpression(e1, line, column), toEntityExpression(e2, line, column), line, column);
	}

	@Override
	public BooleanExpression createInequality(int line, int column, Expression<?> e1,
			Expression<?> e2) {
		return new ExpressionOfNegation(createEquality(line, column, e1, e2), line, column);
	}

	@Override
	public DoubleExpression createAdd(int line, int column, Expression<?> e1,
			Expression<?> e2) {
		return new ExpressionAdd(toDoubleExpression(e1, line, column), toDoubleExpression(e2, line, column), line, column);
	}

	@Override
	public DoubleExpression createSubtraction(int line, int column, Expression<?> e1,
			Expression<?> e2) {
		return new ExpressionSubtraction(toDoubleExpression(e1, line, column), toDoubleExpression(e2, line, column), line, column);
	}

	@Override
	public DoubleExpression createMul(int line, int column, Expression<?> e1,
			Expression<?> e2) {
		return new ExpressionMul(toDoubleExpression(e1, line, column), toDoubleExpression(e2, line, column), line, column);
	}

	@Override
	public DoubleExpression createDivision(int line, int column, Expression<?> e1,
			Expression<?> e2) {
		return new ExpressionDivision(toDoubleExpression(e1, line, column), toDoubleExpression(e2, line, column), line, column);
	}

	@Override
	public DoubleExpression createSqrt(int line, int column, Expression<?> e) {
		return new ExpressionSqrt(toDoubleExpression(e, line, column), line, column);
	}

	@Override
	public DoubleExpression createSin(int line, int column, Expression<?> e) {
		return new ExpressionSin(toDoubleExpression(e, line, column), line, column);
	}

	@Override
	public DoubleExpression createCos(int line, int column, Expression<?> e) {
		return new ExpressionCos(toDoubleExpression(e, line, column), line, column);
	}

	@Override
	public StatementTurn createTurn(int line, int column, Expression<?> angle) {
		return new StatementTurn(toDoubleExpression(angle, line, column), line, column);
	}

	@Override
	public StatementMove createMove(int line, int column) {
		return new StatementMove(line, column);
	}

	@Override
	public StatementJump createJump(int line, int column) {
		return new StatementJump(line, column);
	}

	@Override
	public StatementToggleWeap createToggleWeap(int line, int column) {
		return new StatementToggleWeap(line, column);
	}

	@Override
	public StatementFire createFire(int line, int column, Expression<?> yield) {
		return new StatementFire(toDoubleExpression(yield, line, column), line, column);
	}

	@Override
	public StatementSkip createSkip(int line, int column) {
		return new StatementSkip(line, column);
	}

	@Override
	public StatementAssignment createAssignment(int line, int column,
			String variableName, Expression<?> rhs) {
		return new StatementAssignment(variableName, rhs, line, column);
	}

	@Override
	public StatementIf createIf(int line, int column, Expression<?> condition,
			Statement then, Statement otherwise) {
		return new StatementIf(toBooleanExpression(condition, line, column), then, otherwise, line, column);
	}

	@Override
	public StatementWhile createWhile(int line, int column, Expression<?> condition,
			Statement body) {
		return new StatementWhile(toBooleanExpression(condition, line, column), body, line, column);
	}

	@Override
	public StatementForeach createForeach(int line, int column,
			ForeachType type,
			String variableName, Statement body) {
		return new StatementForeach(type, variableName, body, line, column);
	}

	@Override
	public StatementSequence createSequence(int line, int column,
			List<Statement> statements) {
		return new StatementSequence(statements, line, column);
	}

	@Override
	public StatementPrint createPrint(int line, int column, Expression<?> e) {
		return new StatementPrint(e, line, column);
	}

	@Override
	public Type createDoubleType() {
		return Type.DOUBLE;
	}

	@Override
	public Type createBooleanType() {
		return Type.BOOLEAN;
	}

	@Override
	public Type createEntityType() {
		return Type.ENTITY;
	}

}
