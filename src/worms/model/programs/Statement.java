package worms.model.programs;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import worms.gui.game.IActionHandler;
import worms.model.GameObject;
import worms.model.World;
import worms.model.exceptions.*;
import worms.model.programs.ProgramFactory.ForeachType;
/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
class StatementTurn extends ActionStatement {
	/**
	 * 
	 * @param angle
	 */
	public StatementTurn(DoubleExpression angle, int line, int column) {
		super(line, column);
		this.angle = angle;
	}
	
	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException {
		super.execute(handler, globals);
		if (!handler.turn(globals.getSelf(), angle.eval(globals)))
			throw new IllegalActionException("Cannot turn for " + angle.eval(globals) + " radians, not enough action points.");
		return null;
	}
	
	private DoubleExpression angle;

}

class StatementMove extends ActionStatement {

	public StatementMove(int line, int column) {
		super(line, column);
	}

	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException {
		super.execute(handler, globals);
		if (!handler.move(globals.getSelf()))
			throw new IllegalActionException("Cannot move right now.");
		return null;
	}

}

class StatementJump extends ActionStatement {
	
	public StatementJump(int line, int column) {
		super(line, column);
	}

	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException {
		super.execute(handler, globals);
		if (!handler.jump(globals.getSelf()))
			// either the worm is facing downward or it has no action points left
			throw new IllegalActionException("Either the worm is facing downward or it has no action points left.");
		return null;
	}

}

class StatementToggleWeap extends ActionStatement {
	public StatementToggleWeap(int line, int column) {
		super(line, column);
	}

	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException {
		super.execute(handler, globals);
		handler.toggleWeapon(globals.getSelf());
		return null;
	}
	
}

class StatementFire extends ActionStatement {
	
	public StatementFire(DoubleExpression yield, int line, int column) {
		super(line, column);
		this.yield = yield;
	}

	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException {
		super.execute(handler, globals);
		if (!handler.fire(globals.getSelf(), yield.evalInt(globals)))
			throw new IllegalActionException("Cannot shoot right now.");
		return null;
	}
	
	private DoubleExpression yield;

}

abstract class ActionStatement extends Statement {

	public ActionStatement(int line, int column) {
		super(line, column);
	}
	
	@Override
	public boolean isWellFormed(boolean inForeach) {
		return !inForeach;
	}
}

class StatementSkip extends Statement {
	
	public StatementSkip(int line, int column) {
		super(line, column);
	}

	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException {
		super.execute(handler, globals);
		globals.getSelf().getWorld().startNextTurn();
		throw new ProgramInterruption();
	}

	@Override
	public boolean isWellFormed(boolean inForeach) {
		return inForeach;
	}
}

class StatementAssignment extends Statement {
	
	public StatementAssignment(String variableName, Expression<?> rhs, int line, int column) {
		super(line, column);
		this.key = variableName;
		this.value = rhs;
	}
	
	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws FatalExecutionException, OutOfExecutionsException {
		super.execute(handler, globals);
		try {
			globals.setValue(key, value);
		} catch (RuntimeException ex) {
			throw new FatalExecutionException(this.getLine(), this.getColumn(), ex);
		}
		return null;
	}
	
	private String key;
	private Expression<?> value;
	@Override
	public boolean isWellFormed(boolean inForeach) {
		return true;
	}
	
}

class StatementIf extends Statement {
	
	public StatementIf(BooleanExpression condition, Statement then, 
			Statement otherwise, int line, int column) {
		super(line, column);
		this.condition = condition;
		this.then = then;
		this.otherwise = otherwise;
	}
	
	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException {
		super.execute(handler, globals);
		
		Statement[] a = new Statement[1];
		if (condition.eval(globals))
			a[0] = then;
		else
			a[0] = otherwise;
		
		return a;
	}
	
	private BooleanExpression condition;
	private Statement then;
	private Statement otherwise;
	@Override
	public boolean isWellFormed(boolean inForeach) {
		return then.isWellFormed(inForeach) && otherwise.isWellFormed(inForeach);
	}
}

class StatementWhile extends Statement {
	/**
	 * 
	 * @param condition
	 * @param body
	 */
	public StatementWhile(BooleanExpression condition, Statement body, int line, int column) {
		super(line, column);
		this.condition = condition;
		this.statement = body;
	}
	
	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException {
		super.execute(handler, globals);
		Statement[] a = null;
		if (condition.eval(globals)) {
			a = new Statement[2];
			a[0] = this;
			a[1] = statement;
		}
		
		return a;
	}
	
	@Override
	public boolean isWellFormed(boolean inForeach) {
		return statement.isWellFormed(inForeach);
	}
	
	private Statement statement;
	private BooleanExpression condition;
	
}

class StatementForeach extends Statement {
	/**
	 * 
	 * @param type
	 * @param variableName
	 * @param body
	 */
	public StatementForeach(ForeachType type, String variableName, 
			Statement body, int line, int column) throws NullPointerException {
		super(line, column);
		if (type == null)
			throw new NullPointerException();
		this.type = type;
		this.varName = variableName;
		this.body = body;
	}
	
	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException {
		super.execute(handler, globals);
		
		World world = globals.getWorld();
		
		ArrayList<? extends GameObject> entities = null;
		if (this.type == ForeachType.ANY) {
			entities = world.getAllObjects();
			if (world.getActiveProjectile() != null)
				entities.remove(world.getActiveProjectile());
		} else if (this.type == ForeachType.WORM)
			entities = world.getWorms();
		else if (this.type == ForeachType.FOOD)
			entities = world.getFood();
		
		Statement[] a = new Statement[entities.size() * 2];
		int i = entities.size() * 2 - 1;
		for (GameObject entity : entities) {
			a[i--] = new StatementAssignment(varName, 
					new ExpressionEntityLiteral(entity, this.getLine(), this.getColumn()), this.getLine(), this.getColumn());
			a[i--] = this.body;
		}
		return a;
	}
	
	ForeachType type;
	String varName;
	Statement body;
	@Override
	public boolean isWellFormed(boolean inForeach) {
		return body.isWellFormed(true);
	}
}
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen
 *
 */
class StatementPrint extends Statement {
	/**
	 * 
	 * @param e
	 */
	public StatementPrint(Expression<?> e, int line, int column) {
		super(line, column);
		this.e = e;
	}
	
	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) throws OutOfExecutionsException {
		super.execute(handler, globals);
		System.out.println(e.eval(globals));
		return null;
	}
	
	private Expression<?> e;

	@Override
	public boolean isWellFormed(boolean inForeach) {
		return true;
	}
	
}
/**
 * 
 * @author Thomas
 *
 */
class StatementSequence extends Statement {
	/**
	 * 
	 * @param statements
	 */
	public StatementSequence(List<Statement> statements, int line, int column) {
		super(line, column);
		this.statements = statements;
	}
	
	@Override
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException {
		super.execute(handler, globals);
		// reverse the order of the statements so they are pushed on the stack in the correct order
		Statement[] a = new Statement[statements.size()];
		ListIterator<Statement> iterator = statements.listIterator(statements.size());
		int i = 0;
		while (iterator.hasPrevious())
			a[i++] = iterator.previous();
		return a;
	}
	
	private List<Statement> statements;

	@Override
	public boolean isWellFormed(boolean inForeach) {
		for (Statement s:statements) {
			if (!s.isWellFormed(inForeach))
				return false;
		}
		return true;
	}
}
/**
 * 
 * @author Thomas
 *
 */
public abstract class Statement extends PosHolder {
	
	public Statement(int line, int column) {
		super(line, column);
	}
	
	/**
	 * Check if the program is allowed to do more executions and throw an exception if not.
	 * If no exception is thrown the number of executions left in globals is decremented. 
	 * @return 
	 */
	public Statement[] execute(IActionHandler handler, VariableMap globals) 
			throws OutOfExecutionsException, IllegalActionException, FatalExecutionException {
		if (!globals.hasExecutionsLeft())
			throw new OutOfExecutionsException();
		globals.useExecution();
		return null;
	}
	
	public abstract boolean isWellFormed(boolean inForeach);
	
}
