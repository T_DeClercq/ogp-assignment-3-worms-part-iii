package worms.model;

import worms.model.programs.WormProgramData;
import worms.util.Calculate;
import be.kuleuven.cs.som.annotate.*;
/**
 * @invar	A worms position is a valid position.
 * 			| isValidXCoordinate(getXCoordinate) 
 * 			|	&& isValidYCoordinate(getYCoordinate)
 * @invar	A worms orientation is a valid orientation.
 * 			| isValidOrientation(getOrientation())
 * @invar	A worms radius is a valid radius.
 * 			| isValidRadius(getRadius(), this)
 * @invar	A worms name is a valid name.
 * 			| isValidName(getName())
 * @invar	A worm has a valid amount of actionpoints.
 * 			| isValidActionPoints(getActionPoints(), this)
 * @invar	The minimal radius of a worm is larger than zero.
 * 			| getMinimalRadius() > 0
 * @invar	A worms maximum action points is equal to its mass rounded to the nearest integer.
 * 			| getMaxActionPoints() == (int)Math.round(getMass())
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 */

public class Worm extends OrientedGameObject{
	

	/**
	 * Create a new worm at the given location, 
	 * with the given orientation, radius and name.
	 * @post 	getXCoordinate will return xCoordinate.
	 * 			| new.getXCoordinate() == xCoordinate
	 * @post 	getYCoordinate will return yCoordinate.
	 * 			| new.getYCoordinate() == yCoordinate
	 * @effect 	Set the orientation of this worm to be orientation.
	 * 			| this.setOrientation(orientation)
	 * @effect 	Set the radius of this worm to be radius.
	 * 			| this.setRadius(radius)
	 * @effect 	Give this worm the name 'name'.
	 * 			| this.setName(name)
	 * @effect	Set this worms program to be 'program'.
	 * 			| this.program = program
	 * @param 	xCoordinate
	 * 			| The x-coordinate (horizontal) of this worm.
	 * @param 	yCoordinate
	 * 			| The y-coordinate (vertical) of this worm.
	 * @param 	orientation
	 * 			| The direction of this worm, expressed as an angle in radians.
				  For example, the angle of a worm facing right is 0, a worm facing up is at
				  angle PI/2, a worm facing left is at angle PI and a worm facing down is at angle
				  3PI/2.
	 * @param 	radius
	 * 			| The radius of this worm (expressed in meters) centered on the worm's position.
	 * @param 	name
	 * 			| The name of this worm.
	 * @param program The program this worm will use or null if it will be player controlled.
	 * @throws	IllegalArgumentException
	 * 			If xCoordinate, yCoordinate, radius or name are invalid.
	 * 			| !isValidXCoordinate(xCoordinate) || !isValidYCoordinate(yCoordinate) 
	 * 			|	|| !isValidRadius(radius) || !isValidName(name)
	 * @throws	IllegalArgumentException
	 * 			If world is a null reference.
	 * 			| world == null
	 */
	public Worm(double xCoordinate, double yCoordinate, double orientation, double radius, String name, World world, Program program) throws IllegalArgumentException {
		super(xCoordinate, yCoordinate, orientation, radius, world);
		this.setName(name);
		this.setActionPoints(this.getMaxActionPoints());
		this.setHitPoints(this.getMaxHitPoints());
		this.program = program;
		if (program != null)
			this.programData = new WormProgramData(this, program.getGlobals());
		else
			this.programData = null;
	}	
	
	public void executeProgram() {
		if (!this.hasProgram())
			throw new IllegalStateException(this.getName() + " does not have a program");
		this.getProgram().execute(programData);
	}
	
	@Raw @Immutable
	public boolean hasProgram() {
		return this.program != null;
	}
	
	/** 
	 * Returns the program associated with this worm.
	 * If this worm doesn't have a program this will return null.
	 * @return	This worms program.
	 * 			| result == this.program
	 */
	@Basic @Raw 
	public Program getProgram(){
		return this.program;
	}
	
	private final WormProgramData programData;
	
	@Model
	private final Program program;
	
	// Aspects related to position: Defensively
	
	/**
	 * Get a boolean indicating whether or not this worm can move in its current position.
	 * @return	A boolean indicating if the worm can move the given amount of steps.
	 * 			True if there is a passable location whithin a maximum divergence of 0.7875 radian degrees
	 * 			from the worms current orientation and in a distance greater than MINIMAL_MOVE_DISTANCE
	 * 			and smaller than or equal to the worms radius.
	 * 			| result == (worm.getActionPoints() >= nbSteps * (60/worm.getOrientation()))
	 */
	public boolean canMove() {		
		for (double div = 0; div <= 0.7875; div += 0.0175) {
			// compute expected position for several distance changes
			for (double distance = this.getRadius(); distance >= MINIMAL_MOVE_DISTANCE; distance -= this.getRadius() / 500) {
				for (int sign = -1; sign <= 1; sign += 2) {
					double direction = this.getOrientation() + sign * div;
					double expectedX = this.getXCoordinate() + distance * Math.cos(direction);
					double expectedY = this.getYCoordinate() + distance * Math.sin(direction);
					
					if (this.getWorld().isPassable(expectedX, expectedY, this.getRadius())
							&& this.getActionPoints() >= Math.ceil((Math.abs(Math.cos(direction)) + Math.abs(4*Math.sin(direction))) * (this.getRadius() / distance)))
						return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Move the worm less or more in the direction it is facing.
	 * @throws IllegalStateException 
	 * 			When this worm cannot move.
	 * 			| !this.canMove()
	 * 
	 */
	public void move() throws IllegalArgumentException {		
		if (!this.canMove())
			throw new IllegalStateException("Cannot move.");
		
		double[] bestDivDist = this.findBestAdjacentMoveLocation();
		
		if (bestDivDist == null) {
			bestDivDist = this.findBestPassableMoveLocation();
			assert bestDivDist != null; // if this is null, canMove should have returned false
		}
		
		double bestDiv = bestDivDist[0];
		double bestDist = bestDivDist[1];
		
		double cosine = Math.cos(this.getOrientation() + bestDiv);
		double sine = Math.sin(this.getOrientation() + bestDiv);
		this.setXCoordinate(this.getXCoordinate() + cosine * bestDist);
		this.setYCoordinate(this.getYCoordinate() + sine * bestDist);
		
		double relativeMovement = this.getRadius() / bestDist;
		
		this.useActionPoints((Math.abs(cosine) 
						 + Math.abs(4*sine)) * relativeMovement);
		
		if (!this.getWorld().isOutsideWorld(this.getXCoordinate(), this.getYCoordinate(), this.getRadius()))
			this.tryToEat();
	}
	/**
	 * Find the best adjacent location to move the worm to.
	 * @return Double containing the best Div and the best Distance.
	 */
	private double[] findBestAdjacentMoveLocation() {
		double bestDiv = Double.POSITIVE_INFINITY;
		double bestDist = Double.NEGATIVE_INFINITY;
		
outer: 	for (double div = 0; div <= 0.7875; div += 0.0175) {
			for (double distance = this.getRadius(); distance >= MINIMAL_MOVE_DISTANCE; distance -= this.getRadius() / 500) {
				for (int sign = -1; sign <= 1; sign += 2) {
					double expectedX = this.getXCoordinate() + distance * Math.cos(this.getOrientation() + div * sign);
					double expectedY = this.getYCoordinate() + distance * Math.sin(this.getOrientation() + div * sign);
					if (distance > bestDist && this.getWorld().isAdjacent(expectedX, expectedY, this.getRadius())) {
						bestDiv = div * sign;
						bestDist = distance;
						if (distance == this.getRadius())
							break outer;
						// we can skip other distances since we've found the maximum bestDist for this angle.
						continue outer;
					}
				}
			}
		}
		if (bestDiv == Double.POSITIVE_INFINITY)
			return null;
		double[] result = {bestDiv, bestDist};
		return result;
	}
	
	private double[] findBestPassableMoveLocation() {
		double bestDiv = Double.POSITIVE_INFINITY;
		double bestDist = Double.NEGATIVE_INFINITY;
		outer: 	for (double div = 0; div <= 0.7875; div += 0.0175) {
			for (double distance = this.getRadius(); distance >= MINIMAL_MOVE_DISTANCE; distance -= this.getRadius() / 500) {
				for (int sign = -1; sign <= 1; sign += 2) {
					double expectedX = this.getXCoordinate() + distance * Math.cos(this.getOrientation() + div * sign);
					double expectedY = this.getYCoordinate() + distance * Math.sin(this.getOrientation() + div * sign);
					if (distance > bestDist && this.getWorld().isPassable(expectedX, expectedY, this.getRadius())) {
						bestDiv = div * sign;
						bestDist = distance;
						if (distance == this.getRadius())
							break outer;
						// we can skip other distances since we've found the maximum bestDist for this angle.
						continue outer;
					}
				}
			}
		}
		if (bestDiv == Double.POSITIVE_INFINITY)
			return null;
		double[] result = {bestDiv, bestDist};
		return result;
	}
	/**
	 * A final constant defining the minimal distance a worm can move.
	 */
	private static final double MINIMAL_MOVE_DISTANCE = 0.1;
	
	// Aspects related to direction: Nominally
	
	
	
	/**
	 * Returns a boolean that is true if the worm can turn and false if the worm cannot turn.
	 * @param 	angle 
	 * 			The angle to turn.
	 * @return	True if the worm has enough actionPoints to turn the given angle, false otherwise.
	 * 			| result == (worm.getActionPoints() >= 60 * Math.abs(angle/(2*Math.PI)))
	 */
	public boolean canTurn(double angle) {
		return this.getActionPoints() >= 60 * Math.abs(angle/(2*Math.PI));
	}
	
	/**
	 * Turns the worm with given angle.
	 * @pre		Angle is larger than or equal to 0 and smaller than two times pi.
	 * 			| (angle >= 0 && angle < 2* Math.PI)
	 * @post	This worm will turn by the given angle (in radians).
	 * 			| new.getOrientation() == this.setOrientation(this.getOrientation + angle)
	 * 
	 * @param 	angle	
	 * 			The angle the worm should turn (in radians).
	 */
	public void turn(double angle) {
		double newOrientation = this.getOrientation() + angle;
		
		if (newOrientation < 0.0)
			newOrientation = 2.0 * Math.PI - (Math.abs(newOrientation) % (2.0 * Math.PI));
		if (newOrientation >= 2.0 * Math.PI)
			newOrientation %= 2.0 * Math.PI;
		
		//assert isValidOrientation(newOrientation);
		//Required for programming to be nominal, fix errors?
		assert canTurn(angle);
		this.setOrientation(newOrientation);
		this.useActionPoints(60.0 * Math.abs(angle/(2.0*Math.PI)));
	}
	
	// Aspects related to radius: Defensively
	
	/* (non-Javadoc)
	 * @see worms.model.GameObject#setRadius(double)
	 */
	@Raw @Override
	public void setRadius(double radius) throws IllegalArgumentException {
		if (!isValidRadius(radius, this))
			throw new IllegalArgumentException();
		// Question about static checkers (not overridable...)
		//		POSSIBILITIES:
		//		- check in this class and again in superclasses (check the same thing multiple times!) (current situation)
		//		- in subclasses only check added restrictions (e.g. isValidRadius here-> return radius >= worm.getMinimalRadius()) (seems like best solution)
		//		- don't call super methods and just override the mutator if a checker gets changed (or actually added)
		//		- make checkers dynamic -> overridable
		super.setRadius(radius);
		// To prevent action points from surpassing the maximum value (this is why override is required)
		this.setActionPoints(this.getActionPoints());
	}
	/**
	 * Method to make a worm grow by given factor.
	 * @param factor
	 * 			| The factor in wich this worm should grow.
	 * 			| new.getRadius() == this.getRadius() * factor
	 */
	private void grow(double factor) {
		this.setRadius(this.getRadius() * factor);
	}
	
	/**
	 * Get the minimal possible radius for a worm.
	 * @return 	The minimal radius for this worm.
	 */
	@Basic @Immutable
	public double getMinimalRadius() {
		return this.minimalRadius;
	}
	
	/**
	 * Check whether a worm can have a radius equal to the amount given.
	 * @return	A boolean indicating if radius is a valid radius for the given worm.
	 * 			| result == (isValidNumber(radius) && radius >= worm.getMinimalRadius())
	 */
	public static boolean isValidRadius(double radius, Worm worm) {
		return isValidNumber(radius) 
				&& radius >= worm.getMinimalRadius();
	}
	/**
	 * Constant defining the minimal radius of a worm.
	 */
	private double minimalRadius = 0.25;
	
	// Aspects related to mass: Defensively
	/**
	 * Getter for the mass of a worm.
	 * @return  The mass of this worm.
	 * 			| (DENSITY * (4.0/3.0 * Math.PI * Math.pow(worm.getRadius(), 3)))
	 */
	public double getMass() {
		return DENSITY * (4.0/3.0 * Math.PI * Math.pow(this.getRadius(), 3));
	}
	/**
	 * A final constant defining the density of a worm.
	 */
	private static final double DENSITY = 1062.0;
	
	// Aspects related to action points: Total
	
	/**
	 * Set the actionpoints of a worm to a given amount floored to the nearest integer.
	 * @param actionPoints	The desired amount of actionpoints. 
	 * 						The provided double gets floored to an integer.
	 * @effect	Set the actionpoints to be actionPoints floored to the nearest integer.
	 * 			| this.setActionPoints((int)Math.floor(actionPoints))
	 */
	private void setActionPoints(double actionPoints) {
		int flooredActionPoints = (int)Math.floor(actionPoints);
		setActionPoints(flooredActionPoints);
	}
	
	
	/**
	 * Set the actionpoints of a worm to the given amount.
	 * @param 	actionPoints	
	 * 			The desired amount of actionpoints.
	 * @post 	If actionPoints is a valid amount of actionpoints, the actionpoints
	 * 			of this worm will be set to actionPoints.
	 * 			| if (isValidActionPoints(actionPoints, this))
	 * 				new.getActionPoints() == actionPoints
	 * @post	If actionPoints is smaller than zero, the actionpoints
	 * 			of this worm will be set to zero.
	 * 			| if (actionPoints < 0)
	 * 				new.getActionPoints() == 0
	 * @post	If actionPoints is larger than the maximum amount allowed,
	 * 			the actionpoints of this worm will be set to the maximum.
	 * 			| if (actionPoints > this.getMaxActionPoints())
	 * 				new.getActionPoints() == this.getMaxActionPoints()
	 */
	private void setActionPoints(int actionPoints) {
		actionPoints = Math.max(0, actionPoints);
		actionPoints = Math.min(this.getMaxActionPoints(), actionPoints);
		
		this.actionPoints = actionPoints;
	}
	
	/**
	 * Give the worm its max amount of action points.
	 * @post	The worms action points will be equal to its max action points.
	 * 			| new.getActionPoints() == this.getMaxActionPoints()
	 */
	public void giveMaxActionPoints() {
		this.setActionPoints(this.getMaxActionPoints());
	}
	/**
	 * Method to use an amount of action points of this worm.
	 * @param amount
	 * 		| The amount of action points to be used.
	 *  
	 * @effect 
	 * 		| new.getActionPoints() = this.getActionPoints - amount
	 */
	private void useActionPoints(int amount) {
		this.setActionPoints(this.getActionPoints() - amount);
	}
	
	private void useActionPoints(double amount) {
		this.setActionPoints((double)this.getActionPoints() - amount);
	}
	
	
	/**
	 * Get the number of actionpoints for this worm.
	 * @return 	The current number of action points.
	 */
	@Basic @Raw
	public int getActionPoints() {
		return this.actionPoints;
	}
	
	/**
	 * Check whether a given amount of actionpoints is a valid amount for the given worm.
	 * @param actionPoints 	The amount of action points to check for.
	 * @param worm			The worm to check on.
	 * @return True if the amount of actionpoints equals a value between 0 and the maximum actionpoints for this worm. False otherwise.
	 */
	public static boolean isValidActionPoints(int actionPoints, Worm worm) {
		return actionPoints >= 0 && actionPoints <= worm.getMaxActionPoints();
	}
	
	/**
	 * Get the maximum amount of actionpoints possible.
	 * @return	The maximum number of action points which is equal to the mass rounded to the nearest integer.
	 * 			| result == Math.round(this.getMass())
	 */
	public int getMaxActionPoints() {
		long temp = Math.round(this.getMass()); 
		int maxActionPoints = (int) temp;
		if (maxActionPoints != temp){
			throw new IllegalArgumentException("This worm is too big :(");
		}
		return maxActionPoints;
	}
	
	private int actionPoints;
	
	
	/**
	 * Check whether a given name is a valid name for a worm.
	 * @param 	name	The name to check.
	 * @return 	True if the name is valid, false otherwise. Returns true only if the name is
	 *			at least two characters long and starts with an uppercase letter. [In the
	 *			current version, names can only use letters (both uppercase and lowercase),
	 *			quotes (both single and double) and spaces.]
	 */
	public static boolean isValidName(String name) {
		boolean areValidChars = true;
		
		for (int i = 0; i < name.length(); i++) {
			char checkingChar = name.charAt(i);
			if (!isValidChar(checkingChar)) {
				areValidChars = false;
				break;
			}			
		}
		return (name.length() >= 2 && Character.isUpperCase(name.charAt(0)) && 
				areValidChars);
	}

	/**
	 * Check if the given char is valid in a worms name.
	 * @param 	checkingChar
	 * 			The character to check.
	 * @return	A boolean indicating whether the checkingChar is a legal char for a worm name.
	 */
	private static boolean isValidChar(char checkingChar) {
		return (checkingChar >= 'A' && checkingChar <= 'Z') ||
		(checkingChar >= 'a' && checkingChar <= 'z')
		|| legalChars.contains(String.valueOf(checkingChar));
	}	
	
	private static String legalChars = " '\"";
	
	/**
	 * Get the name for this worm.
	 * @return 	The name of the worm.
	 * 			| result == worm.name
	 */
	@Basic @Raw
	public String getName() {
		return this.name;
	}
	
	/**
	 * Set the name of this worm to be the passed name.
	 * @param name	The new name for this worm.
	 * @post	The worms name will be name.
	 * 			| new.getName() == name
	 * @throws 	IllegalArgumentException
	 * 			Name is not valid.
	 * 			| !isValidName(name);
	 */
	@Raw
	public void setName(String name) throws IllegalArgumentException {
		if (!isValidName(name))
			throw new IllegalArgumentException();
		
		this.name = name;
	}
	
	private String name;
	
	/**
	 * @return	The force exerted on the worm when it jumps.
	 */
	double getForce() {
		double force = (5.0 * this.getActionPoints()) + (this.getMass() * getStandardAccel());
		return force;	
	}
	
	/**
	 * Makes the given worm jump.
	 * @pre	The actionpoints of a worm should be higher than 0.
	 * 		| getActionpoints > 0
	 * @pre The orientation of a worm should be between Pi and two times Pi.
	 * 		| getOrientation > Math.PI && worm.orientation < 2*Math.PI
	 * @effect	Call jump(timeStep) in super class
	 * 			| super.jump(timeStep)
	 * @effect	If the worm is outside of the world after the jump it will be killed.
	 * 			| if (this.getWorld().isOutsideWorld(new.getXCoordinate(), new.getYCoordinate(), new.getRadius()) then
	 * 			|		this.kill()
	 * @effect	If it is not outside of the world after the jump, it will check if it is on food.
	 * 			| if (!this.getWorld().isOutsideWorld(new.getXCoordinate(), new.getYCoordinate(), new.getRadius()) then
	 * 			| 		this.tryToEat()
	 * @post	This worm has zero actionpoints left.
	 * 			| new.getActionPoints == 0
	 * @post	The worm will either be killed because it is outside of the world
	 * 			after the jump or it will be adjacent to impassable terrain.
	 * 			| !new.isAlive() || 
	 * 			|		new.getWorld().isAdjacent(new.getXCoordinate(), new.getYCoordinate(), new.getRadius())
	 * @throws 	IllegalStateException
	 * 			This worm doesn't have any actionpoints or it is facing downward.
	 * 			| this.getActionPoints() <= 0 || 
	 * 			| (this.getOrientation() > Math.PI && this.getOrientation() < (2*Math.PI))
	 */
	public void jump(double timeStep) throws IllegalStateException {
		super.jump(timeStep);
		this.setActionPoints(0);
		if (this.getWorld().isOutsideWorld(this.getXCoordinate(), this.getYCoordinate(), this.getRadius()))
			this.kill();
		else
			this.tryToEat();
	}
	
	/**
	 * Get the time it would take for this worm to jump in its current state.
	 * The jump stops if the worm is outside of the world or if it is 
	 * adjacent to impassable terrain.
	 * @return	If the worm jumps for the resulting time it will afterwards be outside of 
	 * 			the world or it will be adjacent to impassable terrain.
	 * @throws	IllegalStateException
	 * 			If the worm has no action points left.
	 * 			| this.getActionPoints() == 0
	 * @throws IllegalStateException
	 * 			If the worm is facing downward.
	 * 			|this.getOrientation() > PI && this.getOrientation() < 2*PI
	 */
	public double getJumpTime(double timeStep) throws IllegalStateException {
		if (this.getActionPoints() == 0)
			throw new IllegalStateException("No action points to jump.");
		if (this.getOrientation() > Math.PI && this.getOrientation() < (2*Math.PI))
			throw new IllegalStateException("Facing the wrong direction to jump.");
		
		World world = this.getWorld();
		if (world.isImpassable(this.getXCoordinate(), this.getYCoordinate(), this.getRadius()))
			throw new IllegalStateException("Cannot jump from an impassable location.");
		
		double jumpTime = 0;
		double[] jumpStep = this.getJumpStep(jumpTime);
		boolean passable = true;
		while ((passable && world.isPassable(jumpStep[0], jumpStep[1], this.getRadius() * 1.1)
				|| Calculate.distanceBetween(this.getXCoordinate(), this.getYCoordinate(), jumpStep[0], jumpStep[1]) < this.getRadius())
				&& !world.isOutsideWorld(jumpStep[0], jumpStep[1], this.getRadius())) 
		{
			jumpTime += timeStep;
			jumpStep = this.getJumpStep(jumpTime);
			passable = world.isPassable(jumpStep[0], jumpStep[1], this.getRadius());
		}
		
		if (!passable)
			throw new IllegalStateException("Jump target position is not far enough away from initial position.");
		
		return jumpTime;
	}
	
	/**
	 * @return	True if the number is valid, false otherwise.
	 * 			| result == !Double.isNaN(number)
	 */
	public static boolean isValidNumber(double number) {
		return !Double.isNaN(number);
	}

	/**
	 * Returns a boolean indicating if this worm can fall or not.
	 * A worm can fall if it's not adjacent to an impassable location.
	 * @return 	True if this worm is adjacent to an impassable location, false otherwise.
	 * 			| result == !this.getWorld().isAdjacent(this.getXCoordinate(), this.getYCoordinate(), this.getRadius())
	 */
	public boolean canFall() {
		return !this.getWorld().isAdjacent(this.getXCoordinate(), this.getYCoordinate(), this.getRadius());
	}

	/**
	 * Make this worm fall down i.e. move it down until it is adjacent to an impassable 
	 * location and subtract hitpoints relative to the fall distance.
	 * @throws IllegalStateException
	 * 			If this worm can not fall.
	 * 			| !this.canFall()
	 * @post	This worm will be adjacent to an impassable location.
	 * 			| this.getWorld.isAdjacent(this.getXCoordinate(), this.getYCoordinate(), this.getRadius)
	 */
	public void fall() throws IllegalStateException {
		if (!this.canFall())
			throw new IllegalStateException("This worm cannot fall.");
		double fallStep = this.getRadius() / 2000;
		double fallDistance = 0;
		boolean remove = false;
		while (this.getWorld().isPassable(this.getXCoordinate(), this.getYCoordinate() - fallDistance, this.getRadius())) {
			if (this.getWorld().isOutsideWorld(this.getXCoordinate(), this.getYCoordinate() - fallDistance, this.getRadius())) {
				fallDistance += fallStep;
				remove = true;
				break;
			}
			fallDistance += fallStep;
		}
		
		fallDistance -= fallStep;
		this.setYCoordinate(this.getYCoordinate() - fallDistance);
		if (remove) {
			this.kill();
		} else {
			tryToEat();
			this.hurt((int) Math.floor(fallDistance * 3.0));
		}
	}


	/**
	 * @return The current amount of hitpoints of this worm.
	 */
	public int getHitPoints() {
		return this.hitPoints;
	}
	
	/**
	 * Set this worms hitPoints to newHitPoints.
	 * @param newHitPoints	The new value for hitPoints.
	 * @post	If newHitPoints is strictly larger than maxhitpoints, hitpoints will be set to maxhitpoints.
	 * 			| if (newHitPoints > this.getMaxHitPoints())
	 *			|		new.hitPoints == this.getMaxHitPoints();
	 * @post	If newHitPoints is strictly less than 0, hitpoints will be set to 0.
	 * 			| if (newHitPoints < 0)
	 *			|		new.hitPoints == 0;
	 * @post	If newHitPoints is in between 0 and maxhitpoints (both included) hitpoints will be set to newHitPoints.
	 * 			| if (newHitPoints <= this.getMaxHitPoints && newHitPoints > 0)
	 * 			|		new.hitPoints == newHitPoints
	 */
	public void setHitPoints(int newHitPoints) 
	{
		if (newHitPoints > this.getMaxHitPoints())
			this.hitPoints = this.getMaxHitPoints();
		else if (newHitPoints < 0)
			this.hitPoints = 0;
		else
			this.hitPoints = newHitPoints;
	}
	
	/**
	 * @effect	Ceil damage and call this.hurt(int).
	 * 			| this.hurt((int)Math.ceil(damage))
	 */
	public void hurt(double damage) {
		this.hurt((int)Math.ceil(damage));
	}
	
	/**
	 * Hurt this worm by the given amount, then check if it is dead.
	 * If it is, it will be removed from the world.
	 * Use a negative argument to restore hitpoints.
	 * @param damage	The amount of hitpoints to subtract.
	 * @effect	Set the worms hit points to be its current hitpoints minus damage.
	 * 			| this.setHitPoints(this.getHitPoints() - damage)
	 * @post	If the worm is dead after the previous operation it will be removed from the world.
	 * 			| if !new.isAlive() then
	 * 			|	this.getWorld().removeWorm(this)
	 */
	public void hurt(int damage) {
		this.setHitPoints(this.getHitPoints() - damage);
		if (!this.isAlive())
			this.getWorld().removeWorm(this);
	}
	
	/**
	 * Kill this worm. Set its hitpoints to be 0.
	 * @effect Set the hitpoints of this worm to 0.
	 * 			| this.setHitPoints(0)
	 * @effect Remove the worm from the world.
	 * 			| this.getWorld().removeWorm(this)
	 */
	public void kill() {
		this.setHitPoints(0);
		this.getWorld().removeWorm(this);
	}
	
	private int hitPoints;
	
	/**
	 * @return The maximum amount of hit points of this worm, which is equal to the mass rounded to the nearest integer.
	 * 			| result == (int) round(this.getMass())
	 * @throws IllegalArgumentException
	 * 			If the worm is too big, so its rounded mass exceeds the maximum value for an integer.
	 * 			| round(this.getMass()) > Integer.MAX_VALUE
	 */
	public int getMaxHitPoints() {
		long maxHitPoints = Math.round(this.getMass()); 
		if (maxHitPoints > Integer.MAX_VALUE)
			throw new IllegalArgumentException("This worm is too big :(");
		
		return (int) maxHitPoints;
	}

	/**
	 * @return  True if this worm has more than 0 hitpoints, false if it has exactly 0 hitpoints.
	 * 			| result == this.getHitPoints() > 0
	 */
	public boolean isAlive() {
		return this.getHitPoints() > 0;
	}
	
	/**
	 * Set this worms team to be the given team.
	 * @param team	The team which this worm will join.
	 */
	public void setTeam(Team team) {
		this.team = team;
	}
	
	@Model @Basic
	private Team getTeam() {
		return this.team;
	}
	
	/**
	 * @return 	Return the name of the team this worm is in.
	 * 			If this worm is not in a team, returns null.
	 * 			| if (this.getTeam()) == null
	 * 			| 	return null;
	 * 			| else
	 * 			| 	return team.getName()
	 */
	public String getTeamName() {
		Team team = this.getTeam();
		if (team == null)
			return null;
		return team.getName();
	}
	
	/**
	 * @return True if this worm is in a team, false otherwise.
	 */
	public boolean isInTeam() {
		return this.team != null;
	}
	
	private Team team;



	/**
	 * @return A string representing the name of the currently selected weapon.
	 */
	public String getSelectedWeapon() {
		return weapons[currentWeaponIndex];
	}


	/**
	 * Select the next weapon in this worms arsenal.
	 */
	public void selectNextWeapon() {
		currentWeaponIndex++;
		if (currentWeaponIndex >= weapons.length)
			currentWeaponIndex = 0;
	}

	private int currentWeaponIndex = 0;
	private String[] weapons = { "bazooka", "rifle" };
	
	/**
	 * Makes this worm shoot its active weapon with the given propulsion yield.
	 * @throws IllegalStateException
	 * 			If the worm is dead.
	 * 			| !this.isAlive()
	 * @throws IllegalStateException
	 * 			Worm does not have enough action points to fire weapon.
	 * 			| this.getActionPoints() < Weapon.valueOf(this.getSelectedWeapon().toUpperCase()).getFireCost()
	 * @throws IllegalStateException
	 * 			This worm is not on a passable location.
	 * 			| !this.getWorld().isPassable(this.getXCoordinate(), this.getYCoordinate(), this.getRadius())
	 * @throws IllegalStateException
	 * 			There is still another active projectile.
	 * 			| this.getWorld().getActiveProjectile() != null
	 * @post	The cost to fire the selected weapon will be subtracted of this worms action points
	 * 			| new.getActionPoints() == this.getActionPoints() - Weapon.valueOf(this.getSelectedWeapon().toUpperCase()).getFireCost()
	 * @effect	A new projectile will be created with a position just outside the worms perimeter with the same 
	 * 			orientation as the worm and with the given propulsion yield and the selected weapon as parent weapon.
	 * 			| this.getWorld().setActiveProjectile(
	 * 			|	this.getWorld().createProjectile(
	 * 			|		this.getXCoordinate() + Math.cos(this.getOrientation()) * (this.getRadius() + Weapon.valueOf(this.getSelectedWeapon().toUpperCase()).getRadius()),
	 * 			|		this.getYCoordinate() + Math.sin(this.getOrientation()) * (this.getRadius() + Weapon.valueOf(this.getSelectedWeapon().toUpperCase()).getRadius()),
	 * 			|		this.getOrientation(),
	 * 			|		yield,
	 * 			|		Weapon.valueOf(this.getSelectedWeapon().toUpperCase())
	 * 			|	)
	 * 			| )
	 */			
	public void shoot(int yield) throws IllegalStateException {
		Weapon selectedWeapon = Weapon.valueOf(this.getSelectedWeapon().toUpperCase());
		if (!this.isAlive())
			throw new IllegalStateException("Worm is dead. Dead worms can't fire a weapon...");
		if (this.getActionPoints() < selectedWeapon.getFireCost())
			throw new IllegalStateException("Not enough action points to fire weapon.");
		if (!this.getWorld().isPassable(this.getXCoordinate(), this.getYCoordinate(), this.getRadius()))
			throw new IllegalStateException("Worm is not on a passable location.");
		if (this.getWorld().getActiveProjectile() != null)
			throw new IllegalStateException("There is an active projectile, wait until it's destroyed.");
		double distanceFromWorm = this.getRadius() + selectedWeapon.getRadius() + 0.001;
		double projectileX = this.getXCoordinate() + Math.cos(this.getOrientation()) * distanceFromWorm;
		double projectileY = this.getYCoordinate() + Math.sin(this.getOrientation()) * distanceFromWorm;
		
		Projectile projectile = this.getWorld().createProjectile(projectileX, projectileY, this.getOrientation(), yield, selectedWeapon);
		this.getWorld().setActiveProjectile(projectile);
		this.useActionPoints(selectedWeapon.getFireCost());
	}
	
	/**
	 * If this worm overlaps with a food ration, it will eat it.
	 */
	private void tryToEat() {
		boolean tryAgain = false;
		for (Food food : this.getWorld().getFood())
			if (Calculate.overlaps(food, this)) {
				food.setEaten();
				this.getWorld().removeFood(food);
				double prevRadius = this.getRadius();
				this.grow(1.1);
				if (this.getWorld().isImpassable(this.getXCoordinate(),this.getYCoordinate(),this.getRadius())) {
					double[] newLocation = this.getWorld().getClosePassableLocation(this.getXCoordinate(),this.getYCoordinate(),this.getRadius(), prevRadius * 0.1);
					if (newLocation != null) {
						this.setXCoordinate(newLocation[0]);
						this.setYCoordinate(newLocation[1]);
					}
						
				}
				tryAgain = true;
			}
			
		
		if (tryAgain)
			this.tryToEat();
	}
	
}
