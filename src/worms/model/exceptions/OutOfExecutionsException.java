package worms.model.exceptions;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
public class OutOfExecutionsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8417887994165069598L;
	
	public OutOfExecutionsException() {
		super();
	}
}
