package worms.model.exceptions;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
public class IllegalActionException extends RuntimeException {

	public IllegalActionException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6670625108968158135L;

}
