package worms.model.exceptions;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
public class ProgramInterruption extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8979509296641569063L;

}
