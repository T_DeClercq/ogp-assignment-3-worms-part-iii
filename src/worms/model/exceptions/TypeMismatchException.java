package worms.model.exceptions;

import worms.model.programs.Type;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
public class TypeMismatchException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1068369337375892226L;
	
	public TypeMismatchException(String variableName, Type expected, Type actual) {
		super("Variable '" + variableName + "' is of type " + expected 
				+ ", but tried to assign " + actual);
	}
	
}
