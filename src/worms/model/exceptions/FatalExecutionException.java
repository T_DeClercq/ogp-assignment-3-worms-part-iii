package worms.model.exceptions;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
public class FatalExecutionException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1246102970230622756L;
	
	public FatalExecutionException(int line, int column, Throwable cause) {
		this(line, column, cause, "");
	}
	
	public FatalExecutionException(int line, int column, Throwable cause, String message) {
		super(message, cause);
		
		this.line = line;
		this.column = column;
	}
	
	public FatalExecutionException(int line, int column, String message) {
		super(message);
		
		this.line = line;
		this.column = column;
	}

	public int getLine() {
		return this.line;
	}
	
	public int getColumn() {
		return this.column;
	}
	
	private int line;
	private int column;

	public void print() {
		this.printStackTrace();
		String print = "";
		if (this.getCause() != null)
			print = this.getCause().getMessage();
		print += this.getMessage() + "\n\t at line " + getLine() 
				+ "; column " + getColumn();
		System.out.println(print);
	}
	
}
