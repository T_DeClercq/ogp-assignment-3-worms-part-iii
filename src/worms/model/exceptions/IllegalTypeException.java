package worms.model.exceptions;

import worms.model.programs.Type;
/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
public class IllegalTypeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 794862542533203787L;
	
	public IllegalTypeException() {
		super();
	}
	
	public IllegalTypeException(String message) {
		super(message);
	}
	
	public IllegalTypeException(Type type, Type expected) {
		super("Failed casting: tried casting " + type + " to " + expected + ".");
	}
	
	public IllegalTypeException(Type type, Type expected, int line, int column) {
		super("Failed casting: tried casting " + type + " to " + expected + " at line: " + line + "; col: " + column);
	}
}
