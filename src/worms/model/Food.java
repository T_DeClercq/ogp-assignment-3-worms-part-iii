package worms.model;

import be.kuleuven.cs.som.annotate.*;

/**
 * 
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 * @version	1.1
 *
 */
public class Food extends GameObject {
	
	/**
	 * Create a new Food object at the given coordinates, with the given radius.
	 * @effect 	Call the super constructor.
	 * 			| super(xCoordinate, yCoordinate, radius)
	 * @param xCoordinate	The xCoordinate of this Food object.
	 * @param yCoordinate	The yCoordinate of this Food object.
	 * @param radius		The radius of this Food object.
	 */
	public Food(double xCoordinate, double yCoordinate) {
		super(xCoordinate, yCoordinate, getStandardRadius());
		eaten = false;
	}
	
	/* (non-Javadoc)
	 * @see worms.model.GameObject#getRadius()
	 */
	@Override
	public double getRadius() {
		return RADIUS;
	}
	
	public void setEaten() {
		eaten = true;
	}
	
	public boolean isActive() {
		return !eaten;
	}
	
	private boolean eaten;
	
	/**
	 * @return The standard radius of a food ration.
	 */
	@Immutable @Basic
	public static double getStandardRadius() {
		return RADIUS;
	}
	
	private static final double RADIUS = 0.20;

	
	

}
