package worms.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Team {
	
	public Team(String name) throws IllegalArgumentException {
		this.setName(name);
	}
	
	public void addWorm(Worm worm) {
		worms.add(worm);
		worm.setTeam(this);
	}
	
	public Collection<Worm> getLiveWorms() {
		ArrayList<Worm> liveWorms = new ArrayList<>();
		for (Worm worm:worms)
			if (worm.isAlive())
				liveWorms.add(worm);
		
		return liveWorms;
	}
	
	private List<Worm> worms = new ArrayList<>();
	
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) throws IllegalArgumentException {
		if (!isValidName(name))
			throw new IllegalArgumentException("Not a valid team name.");
		this.name = name;
	}
	
	public static boolean isValidName(String name) {
		if (name.length() < 2)
			return false;
		if (!(name.charAt(0) >= 'A' && name.charAt(0) <= 'Z'))
			return false;
		
		for (int i = 1; i < name.length(); i++) {
			if (!((name.charAt(i) >= 'a' && name.charAt(i) <= 'z')
					|| (name.charAt(i) >= 'A' && name.charAt(i) <= 'Z')))
				return false;
		}
		
		return true;
	}
	
	private String name;
	
	public boolean isDestroyed() {
		for (Worm worm:this.worms)
			if (worm.isAlive())
				return false;
		
		return true;
	}
	
	
}
