package worms.model;

import java.util.Map;
import java.util.Stack;

import be.kuleuven.cs.som.annotate.Basic;

import worms.gui.game.IActionHandler;
import worms.model.exceptions.*;
import worms.model.programs.Statement;
import worms.model.programs.Type;
import worms.model.programs.VariableMap;
import worms.model.programs.WormProgramData;

/**
 * 
 * @author Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo https://bitbucket.org/T_DeClercq/ogp-assignment-3-worms-part-iii
 *
 */
public class Program {
	
	private Map<String, Type> globals;
	private final Statement mainStatement;
	private final IActionHandler handler;
	
	
	/**
	 * Create a new program with the given global variables, main statement and action handler.
	 * @param globals A String-Type Map containing the programs global variables.
	 * @param statement The main statement of this program. In most cases this consists of multiple smaller statements.
	 * @param handler The action handler that will be used to simulate actions as if they were executed by user input.
	 */
	public Program(Map<String, Type> globals, Statement statement, IActionHandler handler) {
		this.globals = globals;
		this.mainStatement = statement;
		this.handler = handler;
	}
	
	public Map<String, Type> getGlobals() {
		return globals;
	}

	/**
	 * Execute this program.
	 * @param worm The worm that will execute this program.
	 * @param StatementStack 
	 * @return 
	 */
	public void execute(WormProgramData programData) {
		if (!this.isRunnable())
			return;
		
		Stack<Statement> executionStack = programData.getStatementStack();
		VariableMap varMap = programData.getVarMap();
		
		varMap.restoreExecutions();
		if (executionStack.isEmpty())
			executionStack.push(mainStatement);
		try {
			while (!executionStack.isEmpty()) {
				Statement current = executionStack.pop();
				Statement[] next = current.execute(handler, varMap);
				if (next != null) {
					int i = 0;
					while (i < next.length)
						executionStack.add(next[i++]);
				}
			}
			mainStatement.execute(handler, varMap);
		} catch (FatalExecutionException ex)  {
			this.setNotRunnable();
			ex.print();
		} catch (OutOfExecutionsException ex) {
			handler.print("Did 1000 actions.");
		} catch (IllegalActionException ex) {
			// Not enough action points or unable to do a certain action.
			// details are in the exception
		} catch (ProgramInterruption in) {
			handler.print("Executed skip command.");
		}
	}
	
	/**
	 * Return a boolean indicating if the program has action statements
	 * whithin (directly or indirectly) a foreach loop.
	 */
	public boolean isWellFormed() {
		return mainStatement.isWellFormed(false);
	}
	
	public void setNotRunnable() {
		this.runnable = false;
	}
	
	/**
	 * Returns a boolean indicating if the program this is for can run.
	 */
	@Basic
	public boolean isRunnable() {
		return runnable;
	}
	private boolean runnable = true;
	

}
