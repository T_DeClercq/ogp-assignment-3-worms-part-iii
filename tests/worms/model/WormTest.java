package worms.model;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import worms.model.Worm;
import worms.util.Util;

public class WormTest {
	
	
	private static final double EPS = Util.DEFAULT_EPSILON;
	
	World world;
	
	@Before
	public void setUp() throws Exception {
		world = new World(4, 4, passableMap, new Random());
	}
	
	// X . X X
	// . . . .
	// . . . .
	// X . X X
	private boolean[][] passableMap = new boolean[][] {
			{ false, true, false, false }, { true, true, true, true },
			{ true, true, true, true }, { false, true, false, false } };
	
	@Test
	public void testMoveHorizontal() {
		Worm worm = new Worm(2, 2, 0, 1, "Ed O'Sullivan", world, null);
		worm.move();
		assertEquals(3, worm.getXCoordinate(), EPS);
		assertEquals(2, worm.getYCoordinate(), EPS);
	}
	
	@Test
	public void testMoveVertical() {
		Worm worm = new Worm(1.5, 1.5, Math.PI / 2, 0.5, "Ed O'Sullivan", world, null);
		worm.move();
		assertEquals(1.5, worm.getXCoordinate(), EPS);
		assertEquals(2, worm.getYCoordinate(), EPS);
	}
	
	@Test
	public void testSetOrientation() {
		Worm worm = new Worm(0, 0, Math.PI / 2, 1, "Ed O'Sullivan", world, null);
		assertEquals(worm.getOrientation(), Math.PI / 2, EPS);
		worm.setOrientation(2);
		assertEquals(worm.getOrientation(), 2, EPS);
	}
	
	@Test
	public void testIsValidOrientationCaseTrue() {
		assertTrue(Worm.isValidOrientation(0));
		assertTrue(Worm.isValidOrientation(Math.PI));
		assertTrue(Worm.isValidOrientation(Math.PI * 2 - 1));	
	}
	
	@Test
	public void testIsValidOrientationCaseFalse() {
		assertFalse(Worm.isValidOrientation(-Math.PI * 2 - 0.0001));		
		assertFalse(Worm.isValidOrientation(Math.PI * 2 + 0.0001));
	}
	
	@Test 
	public void canTurnCaseTrue() {
		Worm worm = new Worm(0, 0, 0, 1, "Ed O'Sullivan", world, null);
		assertTrue(worm.canTurn(Math.PI));
		assertTrue(worm.canTurn(-Math.PI));
		assertTrue(worm.canTurn(2 * Math.PI));
	}
	
	@Test
	public void canTurnCaseFalse() {
		Worm worm = new Worm(0, 0, Math.PI / 2, 1, "Ed O'Sullivan", world, null);
		while(worm.canTurn(Math.PI))
			worm.turn(Math.PI);
			
		assertFalse(worm.canTurn(Math.PI));
		assertFalse(worm.canTurn(1));
	}
	
	@Test
	public void testTurnCommonCase() {
		Worm worm = new Worm(0, 0, 0, 1, "Ed O'Sullivan", world, null);
		worm.turn(Math.PI);
		assertEquals(worm.getOrientation(), Math.PI, EPS);
	}
	
	@Test
	public void testTurnCasePast2PiArg() {
		Worm worm = new Worm(0, 0, 0, 1, "Ed O'Sullivan", world, null);
		worm.turn(3 * Math.PI);
		assertEquals(worm.getOrientation(), Math.PI, EPS);
	}
	
	@Test
	public void testTurnCaseNegativeArg() {
		Worm worm = new Worm(0, 0, 0, 1, "Ed O'Sullivan", world, null);
		worm.turn(-Math.PI);
		assertEquals(worm.getOrientation(), 3/2*Math.PI, EPS);
	}
	
	@Test
	public void testSetRadius() {
		Worm worm = new Worm(0, 0, 0, 1, "Ed O'Sullivan", world, null);
		assertEquals(worm.getRadius(), 1, EPS);
		worm.setRadius(15);
		assertEquals(worm.getRadius(), 15, EPS);
		worm.setRadius(worm.getMinimalRadius());
		assertEquals(worm.getRadius(), worm.getMinimalRadius(), EPS);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetRadiusException() {
		Worm worm = new Worm(0, 0, 0, 1, "Ed O'Sullivan", world, null);
		double minimalRadius = worm.getMinimalRadius();
		worm.setRadius(minimalRadius - 0.001);
	}
	
	@Test
	public void testIsValidRadiusCaseTrue() {
		Worm worm = new Worm(0,0,0,1,"Eddy", world, null);
		assertTrue(Worm.isValidRadius(worm.getMinimalRadius(), worm));
		assertTrue(Worm.isValidRadius(20, worm));
	}
	
	@Test
	public void testIsValidRadiusCaseFalse() {
		Worm worm = new Worm(0,0,0,1,"Eddy", world, null);
		assertFalse(Worm.isValidRadius(worm.getMinimalRadius() - 0.001, worm));
		assertFalse(Worm.isValidRadius(-1, worm));
	}
	
	@Test
	public void testSetName() {
		Worm worm = new Worm(0, 0, 0, 1, "Ed O'Sullivan", world, null);
		worm.setName("James");
		assertEquals(worm.getName(), "James");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNameCaseNoCapital() {
		Worm worm = new Worm(0, 0, 0, 1, "Ed O'Sullivan", world, null);
		worm.setName("higgins");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNameCaseIllegalChar() {
		Worm worm = new Worm(0, 0, 0, 1, "Ed O'Sullivan", world, null);
		worm.setName("Dr. Dre");
	}
	@Test
	public void testValidActionPoints(){
		Worm worm = new Worm(0,0,1,1,"Ed O'Sullivan", world, null);
		assertTrue(Worm.isValidActionPoints(1, worm));
	}
	@Test
	public void testInvalidActionPoints(){
		Worm worm = new Worm(0,0,1,1,"Ed O'Sullivan", world, null);
		assertFalse(Worm.isValidActionPoints(worm.getMaxActionPoints()+1, worm));
	}
	@Test
	public void testGetMaxActionPoints(){
		Worm worm = new Worm(0,0,1,1,"Ed O'Sullivan", world, null);
		assertEquals(Math.round(worm.getMass()), worm.getActionPoints(), EPS);
	}
	@Test
	public void testValidNumber(){
		assertTrue(Worm.isValidNumber(20));
		assertTrue(Worm.isValidNumber(Double.POSITIVE_INFINITY));
		assertTrue(Worm.isValidNumber(Double.NEGATIVE_INFINITY));
	}
	@Test
	public void testInvalidNumber(){
		assertFalse(Worm.isValidNumber(Double.NaN));
	}
	@Test
	public void testGetCurrentWeapon() {
		Worm worm = new Worm(0,0,0,0,"Ed O'Sullivan", world, null);
		assertEquals("bazooka", worm.getSelectedWeapon());
		worm.selectNextWeapon();
		assertEquals("rifle", worm.getSelectedWeapon());
	}
	@Test
	public void testIsAlive() {
		Worm worm = new Worm(0,0,0,1,"Ed O'Sullivan", world, null);
		world.addWorm(worm);
		assertTrue(worm.isAlive());
		world.startGame();
		worm.kill(); // worm gets removed from world when it's killed
		assertFalse(worm.isAlive());
		worm.setHitPoints(1);
		assertTrue(worm.isAlive());
	}
	@Test
	public void testFall() {
		Worm worm = new Worm(2.5,2,0,0.2,"Ed O'Sullivan", world, null);
		world.addWorm(worm);
		assertTrue(worm.canFall());
		worm.fall();
		assertEquals(2.5, worm.getXCoordinate(), EPS);
		assertEquals(1.2, worm.getYCoordinate(), EPS);
	}
	@Test
	public void testFallOutWorld() {
		Worm worm = new Worm(1.5,0,0,0.4,"Ed O'Sullivan", world, null);
		world.addWorm(worm);
		world.startGame();
		assertTrue(worm.canFall());
		worm.fall(); // fall out of the world
		assertFalse(world.getWorms().contains(worm));
	}
	
}
