package worms.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import worms.util.Util;

public class WorldTest {

	private static final double EPS = Util.DEFAULT_EPSILON;

	private Random random;

	private World worldStarted;
	private World world;	

	// X X X X
	// . . . .
	// . . . .
	// X X X X
	private boolean[][] passableMap = new boolean[][] {
			{ false, false, false, false }, { true, true, true, true },
			{ true, true, true, true }, { false, false, false, false } };

	@Before
	public void setup() {
		World.setMaxHeight(Double.MAX_VALUE);
		World.setMaxWidth(Double.MAX_VALUE);
		random = new Random(7357);
		worldStarted = new World(4.0, 4.0, passableMap, random);
		Worm fred = new Worm(2, 2, 0, 1.0, "Fred", worldStarted, null);
		worldStarted.addWorm(fred);
		worldStarted.startGame();
		worldStarted.removeWorm(fred);
		world = new World(4.0, 4.0, passableMap, random);
	}
	
	@Test
	public void addTeamTest() {
		worldStarted.addEmptyTeam("Team");
		assertEquals(worldStarted.getTeams().get(0).getName(), "Team");
	}
	
	@Test
	public void addFoodTest() {
		Food food = world.createFood(2, 2);
		world.addFood(food);
		assertEquals(world.getFood().get(0), food);
	}
	
	@Test(expected=IllegalStateException.class)
	public void addFoodStateExceptionTest() {
		Food food = worldStarted.createFood(2, 2);
		worldStarted.addFood(food);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addFoodArgExceptionNullTest() {
		world.addFood(null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void addFoodArgExceptionDuplicateTest() {
		Food food = world.createFood(2, 2);
		world.addFood(food);
		world.addFood(food);		
	}
	
	@Test
	public void addWormTest() {
		Worm worm = world.createWorm(2, 2, 0, 1.0, "Ed", null);
		world.addWorm(worm);
		assertEquals(world.getWorms().get(0), worm);
	}
	
	@Test(expected=IllegalStateException.class)
	public void addWormStateExceptionTest() {
		Worm worm = world.createWorm(2, 2, 0, 1.0, "Ed", null);
		worldStarted.addWorm(worm);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void addWormArgExceptionNullTest() {
		world.addWorm(null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void addWormArgExceptionDuplicateTest() {
		Worm worm = world.createWorm(2, 2, 0, 1.0, "Ed", null);
		world.addWorm(worm);
		world.addWorm(worm);		
	}
	@Test(expected=IllegalArgumentException.class)
	public void addWormArgExceptionOtherWorldTest() {
		Worm worm = worldStarted.createWorm(2, 2, 0, 1.0, "Ed", null);
		world.addWorm(worm);	
	}	
	@Test
	public void createFoodTest() {
		double foodRadius = Food.getStandardRadius();
		Food food = world.createFood(2, 2);
		assertEquals(food.getXCoordinate(), 2, EPS);
		assertEquals(food.getYCoordinate(), 2, EPS);
		assertEquals(food.getRadius(), foodRadius, EPS);		
		
		food = world.createFood(foodRadius, foodRadius);
		food = world.createFood(4 - foodRadius, 4 - foodRadius);
	}
	@Test(expected=IllegalArgumentException.class)
	public void createFoodTestIllegalPosition() {
		world.createFood(-1, 2);
	}	
	@Test
	public void createProjectileTest() {
		Projectile p = worldStarted.createProjectile(2.5, 2.5, 0, 50, Weapon.BAZOOKA);
		assertEquals(p.getXCoordinate(), 2.5, EPS);
		assertEquals(p.getYCoordinate(), 2.5, EPS);
		// test the rest in projectile test
	}
	@Test
	public void createWormTest() {
		Worm w = world.createWorm(2.5, 2.5, 0, 0.5, "Ed", null);
		assertEquals(w.getXCoordinate(), 2.5, EPS);
		assertEquals(w.getYCoordinate(), 2.5, EPS);
		assertEquals(w.getRadius(), 0.5, EPS);
		assertEquals(w.getName(), "Ed");
	}
	@Test
	public void activeProjectileTest() {
		assertEquals(worldStarted.getActiveProjectile(), null);
		
		Projectile p = worldStarted.createProjectile(2.5, 2.5, 0, 50, Weapon.BAZOOKA);
		worldStarted.setActiveProjectile(p);
		assertEquals(worldStarted.getActiveProjectile(), p);
	}
	@Test
	public void distToPixel() {
		boolean[][] passableMap = new boolean[100][100];
		World world = new World(20, 20, passableMap, random);
		
		assertEquals(Math.sqrt(world.distToPixelSq(0, 0, 0, 20)), 4, EPS);
	}
	@Test
	public void getAllObjectsTest() {
		Worm ed = world.createWorm(2, 2, 0, 1.0, "Ed", null);
		world.addWorm(ed);
		Worm ted = world.createWorm(2, 2, 0, 1.0, "Ted", null);
		world.addWorm(ted);
		ted.kill();
		Food food = world.createFood(2, 2);
		world.addFood(food);
		
		world.startGame();
		
		ArrayList<GameObject> allObjects = world.getAllObjects();
		assertEquals(allObjects.size(), 2);
		for (GameObject object:allObjects)
			assertTrue(object == ed || object == food);
		
		Projectile p = world.createProjectile(2.5, 2.5, 0, 50, Weapon.BAZOOKA);
		world.setActiveProjectile(p);
		
		allObjects = world.getAllObjects();
		assertEquals(allObjects.size(), 3);
		for (GameObject object:allObjects)
			assertTrue(object == ed 
						|| object == food || object == p);
	}
	@Test
	public void getCurrentWormTest() {
		Worm ed = world.createWorm(2, 2.5, 0, 0.5, "Ed", null);
		Worm ted = world.createWorm(3, 2.5, 0, 0.5, "Ted", null);
		world.addWorm(ed);
		world.addWorm(ted);
		assertEquals(world.getCurrentWorm(), null);
		world.startGame();
		assertEquals(world.getCurrentWorm(), ed);
		world.startNextTurn();
		assertEquals(world.getCurrentWorm(), ted);
		world.startNextTurn();
		assertEquals(world.getCurrentWorm(), ed);
	}
	@Test
	public void getHeightTest() {
		assertEquals(world.getHeight(), 4, 0.000001);
	}
	@Test
	public void getDimInPixelsTest() {
		boolean[][] passableMap = { { true, true, true },
									{ true, true , true} };
		World world = new World(10, 50, passableMap, random);
		assertEquals(world.getHeightInPixels(), 2);
		assertEquals(world.getWidthInPixels(), 3);
	}
	@Test
	public void getWidthTest() {
		assertEquals(world.getWidth(), 4, 0.000001);
	}
	@Test
	public void getIndependentWormsTest() {
		Worm ed = world.createWorm(2, 2.5, 0, 0.5, "Ed", null);
		Worm ted = world.createWorm(3, 2.5, 0, 0.5, "Ted", null);
		Worm red = world.createWorm(2, 2.5, 0, 0.5, "Red", null);
		Worm tommy = world.createWorm(3, 2.5, 0, 0.5, "Tommy", null);
		Worm lommy = world.createWorm(3, 2.5, 0, 0.5, "Lommy", null);
		
		world.addWorm(ed);
		world.addWorm(red);
		
		world.addEmptyTeam("TeamOne");
		world.addEmptyTeam("TeamTwo");
		world.addWorm(tommy);
		world.addEmptyTeam("TeamThree");
		world.addWorm(ted);
		world.addWorm(lommy);
		
		ArrayList<Worm> indWorms = world.getIndependentWorms();
		assertTrue(indWorms.contains(ed));
		assertTrue(indWorms.contains(red));
		assertEquals(indWorms.size(), 2);
	}
	@Test
	public void getWormsTest() {
		Worm ed = world.createWorm(2, 2.5, 0, 0.5, "Ed", null);
		Worm ted = world.createWorm(3, 2.5, 0, 0.5, "Ted", null);
		Worm red = world.createWorm(2, 2.5, 0, 0.5, "Red", null);
		Worm tommy = world.createWorm(3, 2.5, 0, 0.5, "Tommy", null);
		Worm lommy = world.createWorm(3, 2.5, 0, 0.5, "Lommy", null);
		
		Worm[] worms = {ed, ted, red, tommy, lommy};
		
		world.addWorm(ed);
		world.addWorm(red);
		
		world.addEmptyTeam("TeamOne");
		world.addEmptyTeam("TeamTwo");
		world.addWorm(tommy);
		world.addEmptyTeam("TeamThree");
		world.addWorm(ted);
		world.addWorm(lommy);
		world.startGame();
		
		ArrayList<Worm> worldWorms = world.getWorms();
		for (Worm worm:worms) {
			assertTrue(worldWorms.contains(worm));
		}

		ed.kill();
		tommy.kill();
		
		worldWorms = world.getWorms();
		assertTrue(worldWorms.contains(ted));
		assertTrue(worldWorms.contains(red));
		assertTrue(worldWorms.contains(lommy));
		assertEquals(worldWorms.size(), 3);
	}
	@Test
	public void getRandomAdjacentPosTest() {
		double[] randomPos = world.getRandomAdjacentPos(1);
		assertTrue(world.isAdjacent(randomPos[0], randomPos[1], 1));
	}
	@Test
	public void getTeamsTest() {
		world.addEmptyTeam("TeamOne");
		world.addEmptyTeam("TeamTwo");
		
		ArrayList<Team> teams = world.getTeams();
		for (Team team:teams)
			assertTrue(team.getName() == "TeamOne" || team.getName() == "TeamTwo");
		assertTrue(teams.size() == 2);
		
	}
	@Test
	public void getWinnerTestTeam() {
		Worm ed = world.createWorm(2, 2.5, 0, 0.5, "Ed", null);
		Worm ted = world.createWorm(3, 2.5, 0, 0.5, "Ted", null);
		Worm red = world.createWorm(2, 2.5, 0, 0.5, "Red", null);
		Worm tommy = world.createWorm(3, 2.5, 0, 0.5, "Tommy", null);
		Worm lommy = world.createWorm(3, 2.5, 0, 0.5, "Lommy", null);
				
		world.addWorm(ed);
		world.addWorm(red);
		
		world.addEmptyTeam("TeamOne");
		world.addEmptyTeam("TeamTwo");
		world.addWorm(tommy);
		world.addEmptyTeam("TeamThree");
		world.addWorm(ted);
		world.addWorm(lommy);
		
		world.startGame();
		
		assertTrue(world.getWinner() == null);
		tommy.kill();
		assertTrue(world.getWinner() == null);
		red.kill();
		ed.kill();
		assertEquals(world.getWinner(), "TeamThree");
		ted.kill();
		assertEquals(world.getWinner(), "TeamThree");
		lommy.kill();
		assertEquals(world.getWinner(), null);	
	}
	@Test
	public void getWinnerTestIndependent() {
		Worm ed = world.createWorm(2, 2.5, 0, 0.5, "Ed", null);
		world.addWorm(ed);
		assertEquals("Ed", world.getWinner());
	}
	@Test
	public void isAdjacentTrueTest() {
		assertTrue(world.isAdjacent(2, 2, 1));
		assertTrue(world.isAdjacent(2, 2, 0.91));
	}
	@Test
	public void isAdjacentFalseTest() {
		assertFalse(world.isAdjacent(2, 2, 0.2));
		assertFalse(world.isAdjacent(0.5, 2, 0.45));
		assertFalse(world.isAdjacent(2, 2, 0.9));		
	}
	@Test
	public void isGameFinishedTest() {
		assertEquals(world.getWorms().isEmpty() || world.getWinner() != null, world.isGameFinished());
	}
	//0 1 2 3 4 
	// X X X X	0.5
	// . . . .	1.5
	// . . . .	2.5
	// X X X X	3.5
	@Test
	public void isImpassibleTrueTest() {
		assertTrue(world.isImpassable(2, 2, 1.0001));
		assertTrue(world.isImpassable(1, 1, 0.001));
		assertTrue(world.isImpassable(2, 2, 1.1));
		
//		. . . .
//		. . X .
//		. . . .
//		. . . .
		boolean[][] passableMap = new boolean[][]{ { true, true, true, true },
													{ true, true, true, true},
													{ true, true, false, true},
													{true, true, true, true } };
		World newWorld = new World(4, 4, passableMap, random);
		assertTrue(newWorld.isImpassable(1.5, 2.5, 0.7075));
		assertTrue(newWorld.isImpassable(2.5, 1.5, 0.0025));
	}
	@Test
	public void isImpassibleFalseTest() {
		assertFalse(world.isImpassable(2, 2, 1.0));
		assertFalse(world.isImpassable(0.5, 2, 0.1));
		assertFalse(world.isImpassable(0.5, 2, 0.6));

//		. . . .
//		. . . .
//		. . X .
//		. . . .
		boolean[][] passableMap = new boolean[][]{ { true, true, true, true },
													{ true, true, true, true},
													{ true, true, false, true},
													{true, true, true, true } };
		World newWorld = new World(16, 16, passableMap, random);
		assertFalse(newWorld.isImpassable(6, 10, 2.82));
	}
	//0 5 10 15 20 
	// X X  X  X	2.5
	// . .  .  .	7.5
	// . .  .  .	12.5
	// X X  X  X	17.5
	@Test
	public void isImpassibleScale5TrueTest() {
		World world = new World(20, 20, passableMap, random);
		assertTrue(world.isImpassable(10, 10, 5.0001));
		assertTrue(world.isImpassable(5, 5, 0.001));
		assertTrue(world.isImpassable(10, 10, 5.5));
	}
	@Test
	public void isImpassibleScale5FalseTest() {
		World world = new World(20, 20, passableMap, random);
		assertFalse(world.isImpassable(10, 10, 5.0));
		assertFalse(world.isImpassable(2.5, 10, 0.5));
		assertFalse(world.isImpassable(2.5, 10, 3.0));
	}
	//0 .25 .5 .75 1 
	// X   X  X   X		0.125
	// .   .  .   .		0.375
	// .   .  .   .		0.625
	// X   X  X   X		0.875
	@Test
	public void isImpassibleScale1on4TrueTest() {
		World world = new World(1, 1, passableMap, random);
		assertTrue(world.isImpassable(0.5, 0.5, 0.25001));
		assertTrue(world.isImpassable(0.25, 0.25, 0.001));
		assertTrue(world.isImpassable(0.5, 0.5, 0.275));
	}
	@Test
	public void isImpassibleScale1on4FalseTest() {
		World world = new World(1, 1, passableMap, random);
		assertFalse(world.isImpassable(0.5, 0.5, 0.25));
		assertFalse(world.isImpassable(0.125, 0.5, 0.025));
		assertFalse(world.isImpassable(0.1, 0.4, 0.12));
	}
	@Test
	public void isInWorldTest() {
		assertTrue(world.isInWorld(0,0));
		assertTrue(world.isInWorld(4,4));
		assertTrue(world.isInWorld(1, 3));
		assertFalse(world.isInWorld(-0.01, -0.01));
		assertFalse(world.isInWorld(1, -0.01));
		assertFalse(world.isInWorld(1, 4.001));
		assertFalse(world.isInWorld(-0.01,2));
		assertFalse(world.isInWorld(2,-0.01));
		assertFalse(world.isInWorld(4.001,2));
		assertFalse(world.isInWorld(4.001,4.001));
	}
	@Test
	public void isInWorldRadiusTest() {
		assertTrue(world.isInWorld(2,2,1));
		assertTrue(world.isInWorld(1,1,1));
		assertTrue(world.isInWorld(3,3,1));
		assertFalse(world.isInWorld(2,2,2.01));
		assertFalse(world.isInWorld(-0.5,-0.5,1));
		assertFalse(world.isInWorld(4,4,0.01));
		assertFalse(world.isInWorld(1.5, 3, 1.5));
		assertFalse(world.isInWorld(2, 1, 1.5));
	}
	@Test
	public void isOutsideWorldTest() {
		assertTrue(world.isOutsideWorld(-1, -1, 0.99));
		assertTrue(world.isOutsideWorld(5.1, 5.1, 1));
		assertFalse(world.isOutsideWorld(4.5, 2, 1));
		assertFalse(world.isOutsideWorld(2, 4.5, 1));
		assertFalse(world.isOutsideWorld(2, 2, 2));
		assertFalse(world.isOutsideWorld(1, 3, 1));
	}
	@Test
	public void isPixelOutsideWorldTest() {
		assertTrue(world.isPixelOutsideWorld(-1, 1));
		assertTrue(world.isPixelOutsideWorld(1, -1));
		assertTrue(world.isPixelOutsideWorld(5, 1));
		assertTrue(world.isPixelOutsideWorld(1, 5));
		assertFalse(world.isPixelOutsideWorld(0, 0));
	}
	@Test
	public void teamsLeftTest() {
		assertEquals(0, world.nbTeamsLeft());
		
		Worm ed = world.createWorm(2, 2.5, 0, 0.5, "Ed", null);
		Worm ted = world.createWorm(3, 2.5, 0, 0.5, "Ted", null);
		Worm red = world.createWorm(2, 2.5, 0, 0.5, "Red", null);
		Worm tommy = world.createWorm(3, 2.5, 0, 0.5, "Tommy", null);
		Worm lommy = world.createWorm(3, 2.5, 0, 0.5, "Lommy", null);
		
		world.addWorm(ed);
		world.addWorm(red);
		
		world.addEmptyTeam("TeamOne");
		world.addEmptyTeam("TeamTwo");
		world.addWorm(tommy);
		world.addEmptyTeam("TeamThree");
		world.addWorm(ted);
		world.addWorm(lommy);
		
		assertEquals(world.nbTeamsLeft(), 2);
		for (Team team:world.teamsLeft())
			assertTrue(team.getName() == "TeamTwo" || team.getName() == "TeamThree");
		
		ted.kill();
		tommy.kill();
		assertEquals(world.nbTeamsLeft(), 1);
		assertEquals(world.teamsLeft().get(0).getName(), "TeamThree");
	}
	@Test
	public void removeActiveProjectileTest() {
		worldStarted.setActiveProjectile(worldStarted.createProjectile(2, 2, 0, 0, Weapon.BAZOOKA));
		assertTrue(worldStarted.getActiveProjectile() != null);
		worldStarted.removeActiveProjectile();
		assertTrue(worldStarted.getActiveProjectile() == null);
	}
	@Test(expected=IllegalStateException.class)
	public void removeActiveProjectileTestException() {
		world.removeActiveProjectile();
	}
	@Test
	public void removeFoodTest() {
		Food food = world.createFood(2,2);
		world.addFood(food);
		world.removeFood(food);
		assertEquals(0, world.getFood().size());
	}
	@Test(expected=IllegalArgumentException.class)
	public void removeFoodTestException() {
		Food food = world.createFood(2,2);
		world.removeFood(food);
	}
	@Test
	public void removeWormTest() {
		//Assuming addWorm works
		Worm worm = world.createWorm(10, 10, 1, 1, "Ed", null);
		world.addWorm(worm);
		world.startGame();
		world.removeWorm(worm);
		assertEquals(0, world.getWorms().size());
	}
	@Test(expected=IllegalArgumentException.class)
	public void removeWormTestException() {
		//Assuming addWorm works
		Worm worm = world.createWorm(10, 10, 1, 1, "Ed", null);
		world.addWorm(worm);
		world.startGame();
		world.removeWorm(worm);
		world.removeWorm(worm);
	}
	@Test
	public void toMetersTest() {
		assertEquals(world.toMeters(20), 20, EPS);
		// scale is 5m/p
		World w = new World(20, 20, passableMap, random);
		assertEquals(w.toMeters(1), 5, EPS);
		
	}
	@Test
	public void toPixelsTest() {
		assertEquals(world.toPixels(20), 20, EPS);
		// scale is 5m/p
		World w = new World(20, 20, passableMap, random);
		assertEquals(w.toPixels(5), 1, EPS);
	}
	@Test
	public void isValidDimensionTrueTest() {
		assertTrue(World.isValidDimension(100, 100));
		assertTrue(World.isValidDimension(0, 0));
		assertTrue(World.isValidDimension(Double.MAX_VALUE, Double.MAX_VALUE));
	}
	@Test
	public void isValidDimensionFalseTest() {
		assertFalse(World.isValidDimension(-0.1, 0));
		assertFalse(World.isValidDimension(0, -0.1));
		assertFalse(World.isValidDimension(Double.NaN, 0));
	}
	@Test
	public void setMaxHeightTest() {
		assertTrue(World.isValidDimension(200, 201));
		World.setMaxHeight(200);
		assertFalse(World.isValidDimension(200, 201));
	}
	@Test
	public void setMaxWidthTest() {
		assertTrue(World.isValidDimension(201, 200));
		World.setMaxWidth(200);
		assertFalse(World.isValidDimension(201, 200));
	}
	@Test(expected=IllegalArgumentException.class)
	public void setMaxHeightTestException() {
		// we know it uses isValidDimension, so just one check is enough since isValidDimension is checked separately.
		World.setMaxHeight(-1);
	}
	@Test(expected=IllegalArgumentException.class)
	public void setMaxWidthTestException() {
		World.setMaxWidth(-1);
	}
}
