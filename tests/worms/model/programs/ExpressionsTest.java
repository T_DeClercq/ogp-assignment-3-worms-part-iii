package worms.model.programs;

import static org.junit.Assert.*;

import org.junit.Test;
import worms.model.Worm;

public class ExpressionsTest {
	
	@Test
	public void testBooleanLiteral () {
		BooleanExpression b = new ExpressionBooleanLiteral(false, 0, 0);
		assertFalse(b.eval(null));
		b = new ExpressionBooleanLiteral(true, 0, 0);
		assertTrue(b.eval(null));
	}
	
	@Test
	public void testConjunction() {
		BooleanExpression b1 = new ExpressionBooleanLiteral(true, 0, 0);
		BooleanExpression b2 = new ExpressionBooleanLiteral(true, 0, 0);
		assertTrue((b1.eval(null) && b2.eval(null)));
	}
	
	@Test
	public void testDisjunction() {
		BooleanExpression b1 = new ExpressionBooleanLiteral(false, 0, 0);
		BooleanExpression b2 = new ExpressionBooleanLiteral(true, 0, 0);
		assertFalse((b1.eval(null) && b2.eval(null)));
	}
	@Test
	public void testNegation() {
		BooleanExpression b = new ExpressionBooleanLiteral(false, 0, 0);
		assertTrue(!(b.eval(null)));
	}
	@Test
	public void testLessThan() {
		DoubleExpression d1 = new ExpressionDoubleLiteral(5, 0, 0);
		DoubleExpression d2 = new ExpressionDoubleLiteral(8, 0, 0);
		assertTrue(new ExpressionLessThan(d1, d2, 0, 0).eval(null));
	}
	@Test
	public void testNotLessThan() {
		DoubleExpression d1 = new ExpressionDoubleLiteral(8, 0, 0);
		DoubleExpression d2 = new ExpressionDoubleLiteral(5, 0, 0);
		assertFalse(new ExpressionLessThan(d1, d2, 0, 0).eval(null));
	}
	@Test
	public void testGreaterThan() {
		DoubleExpression d1 = new ExpressionDoubleLiteral(8, 0, 0);
		DoubleExpression d2 = new ExpressionDoubleLiteral(5, 0, 0);
		assertTrue(new ExpressionGreaterThan(d1, d2, 0, 0).eval(null));
	}
	@Test
	public void testNotGreaterThan() {
		DoubleExpression d1 = new ExpressionDoubleLiteral(5, 0, 0);
		DoubleExpression d2 = new ExpressionDoubleLiteral(8, 0, 0);
		assertFalse(new ExpressionGreaterThan(d1, d2, 0, 0).eval(null));
	}
	
	@Test
	public void testLessThanOrEqual() {
		DoubleExpression d1 = new ExpressionDoubleLiteral(5, 0, 0);
		DoubleExpression d2 = new ExpressionDoubleLiteral(5, 0, 0);
		assertTrue(new ExpressionLessThanOrEqualTo(d1, d2, 0, 0).eval(null));
	}
	
	@Test
	public void testGreaterThanOrEqual() {
		DoubleExpression d1 = new ExpressionDoubleLiteral(5, 0, 0);
		DoubleExpression d2 = new ExpressionDoubleLiteral(5, 0, 0);
		assertTrue(new ExpressionGreaterThanOrEqualTo(d1, d2, 0, 0).eval(null));
	}
	
	@Test
	public void testEquality1() {
		BooleanExpression b1 = new ExpressionBooleanLiteral(false,0,0);
		BooleanExpression b2 = new ExpressionBooleanLiteral(false,0,0);
		assertTrue(new ExpressionEquality<BooleanExpression>(b1,b2,0,0).eval(null));
	}

	@Test
	public void testEquality2() {
		DoubleExpression d1 = new ExpressionDoubleLiteral(5, 0, 0);
		DoubleExpression d2 = new ExpressionDoubleLiteral(5, 0, 0);
		assertTrue(new ExpressionEquality<DoubleExpression>(d1, d2, 0, 0).eval(null));
	}
	
	@Test
	public void testEquality3() {
		Worm gameObject = new Worm(0, 0, 0, 0, null, null, null);
		EntityExpression e1 = new ExpressionEntityLiteral(gameObject, 0, 0);
		EntityExpression e2 = new ExpressionEntityLiteral(gameObject, 0, 0);
		assertTrue(new ExpressionEquality<EntityExpression>(e1,e2,0,0).eval(null));
	}	
}
